/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.mediator
{
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mxml.TimelineView;
	import flash.events.Event;
	import flash.utils.setTimeout;
	import mx.collections.ArrayCollection;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ShotTimelineMediator extends Mediator
	{
		public static const NAME:String = "timeLineMediator";
		private var spaceBarPressed:Boolean = false;
		public var _frameArrayCollection:ArrayCollection;
		
		public function ShotTimelineMediator(viewComponent:Object=null, mediatorName:String=NAME) {
			super(mediatorName, viewComponent);
			timelineView.addEventListener(TimelineView.EVENT_SELECT_FRAME_CHANGED, onCurrentFrameChanged);
			timelineView.addEventListener(TimelineView.EVENT_COPY, onCopy);
			timelineView.addEventListener(TimelineView.EVENT_CUT, onCut);
			timelineView.addEventListener(TimelineView.EVENT_DELETE, onDelete);
			//timelineView.addEventListener(TimelineView.EVENT_IMPORT, onImport);
			timelineView.addEventListener(TimelineView.EVENT_PASTE_BEFORE, onPasteBefore);
			timelineView.addEventListener(TimelineView.EVENT_PASTE_AFTER, onPasteAfter);
			timelineView.addEventListener(TimelineView.EVENT_REVERSE_PASTE, onReversePaste);
			timelineView.addEventListener(TimelineView.EVENT_REVERSE, onReverse);
			timelineView.addEventListener(TimelineView.EVENT_DUPLICATE, onDuplicate);
		}
		
		/* Liste des notifications prises en compte par ce mediator */
		override public function listNotificationInterests():Array
		{
			return [
				ShotConstant.LOAD_FRAMES,
				ShotConstant.LOAD_FRAMES_SUCCESS,
				ShotConstant.CAPTURE_FRAMES_SUCCESS,
				ShotConstant.CURRENT_FRAME_CHANGED,
				ShotConstant.TIMELINE_ZOOMED_IN,
				ShotConstant.TIMELINE_ZOOMED_OUT,
				ShotConstant.LIVE_VIDEO_SHOWN
			];
		}
		
		/* Gestion des notifiactions de l'application */
		override public function handleNotification(note:INotification):void {
			var name:String = note.getName();
			switch(name) {
				case ShotConstant.CAPTURE_FRAMES_SUCCESS:
					//	sendNotification(AppFacade.GOTO_LAST_FRAME);
					break;
				case ShotConstant.CURRENT_FRAME_CHANGED:
					changeFrame(note.getBody() as int);
					break;
				case ShotConstant.TIMELINE_ZOOMED_OUT :
				case ShotConstant.TIMELINE_ZOOMED_IN :
					timelineView.refresh();
					break;
			}
		}
		
		/* Mise a jour de la liste des Thumb */
		public function set frameArrayCollection(frameArrayCollection:ArrayCollection):void {	
			this._frameArrayCollection=frameArrayCollection;
			//list est une référence direct au data du proxy, donc
			//tout ajout de vue dans le proxy est répercuté dans le dataViewsList.
			timelineView.frameArrayCollection=frameArrayCollection;
		}
		
		/* Gestion d'une notification de changement du thumb selectionné */
		private function changeFrame(index:int):void { 
			setTimeout(updatePosition,1,index);
		}
		
		/* Mise a jour de la position de la timeline */
		private function updatePosition(index:int):void{
			if (index>=0 && timelineView.list) {
				timelineView.list.ensureIndexIsVisible(index);
				timelineView.list.selectedIndex = index;
				timelineView.list.setFocus();
			}
		}
		
		public function updateSelectedIndices(selectList:Vector.<int>):void {
			timelineView.list.selectedIndices = selectList;
			if (selectList[0]+1<timelineView.list.dataProvider.length){
				timelineView.list.ensureIndexIsVisible(selectList[0]+1);
			} else {
				timelineView.list.ensureIndexIsVisible(selectList[0]);
			}
		}
		
		public function set selectedIndices ( selectList:Vector.<int> ):void {
			setTimeout(updateSelectedIndices,1,selectList);
		}
		
		/* Gestion des evenements UI */
		private function onCopy(e:Event):void { sendNotification(ShotConstant.COPY_FRAMES); }
		private function onPasteBefore(e:Event):void { sendNotification(ShotConstant.PASTE_FRAMES_BEFORE); }
		private function onPasteAfter(e:Event):void { sendNotification(ShotConstant.PASTE_FRAMES_AFTER); }
		private function onCut(e:Event):void { sendNotification(ShotConstant.CUT_FRAMES); }
		private function onDelete(e:Event):void { sendNotification(ShotConstant.DELETE_FRAMES); }
		private function onReverse(e:Event):void { sendNotification(ShotConstant.REVERSE_FRAMES); }
		private function onReversePaste(e:Event):void { sendNotification(ShotConstant.PASTE_INVERTED_FRAMES) }
		private function onImport(e:Event):void { sendNotification(ShotConstant.IMPORT_IMAGE); }
		private function onDuplicate(e:Event):void { sendNotification(ShotConstant.DUPLICATE_FRAMES); }
		
		private function onCurrentFrameChanged(e:Event):void {  
			sendNotification(ShotConstant.TAKETIMELINEMEDIATOR_FRAME_CHANGED, timelineView.list.selectedIndex); 
		}
		
		/* Accesseurs */
		public function get timelineView():TimelineView { return viewComponent as TimelineView;  }
		public function get selectedItems():Vector.<Object> { return timelineView.list.selectedItems; }
		public function get selectedIndices():Vector.<int> { return timelineView.list.selectedIndices; }
	}
}