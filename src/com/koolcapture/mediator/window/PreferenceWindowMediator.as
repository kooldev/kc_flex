/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.mediator.window
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.event.ObjectEvent;
	import com.koolcapture.model.PreferencesProxy;
	import com.koolcapture.mxml.window.PreferencesWindow;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NativeWindowBoundsEvent;
	
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import spark.events.TitleWindowBoundsEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class PreferenceWindowMediator extends Mediator
	{
		public static const NAME:String="PreferenceWindowMediator";
		protected var app:KoolCapture;
		protected var preferenceWindow:PreferencesWindow;
		protected var preferenceWindowInitialised:Boolean;
		
		public function PreferenceWindowMediator(viewComponent:Object=null, mediatorName:String=NAME) {
			super(mediatorName, viewComponent);
			this.app = KoolCapture (viewComponent);
			this.app.addEventListener(NativeWindowBoundsEvent.RESIZING, windowResizing);
			this.preferenceWindow = KoolCapture (viewComponent).preferenceWindow;
			this.preferenceWindow.addEventListener(PreferencesWindow.EVENT_WORKSPACE_CHANGED, workSpaceChanged);
			this.preferenceWindow.addEventListener(PreferencesWindow.LANGUAGECHANGED, languageChanged);
			this.preferenceWindow.addEventListener(TitleWindowBoundsEvent.WINDOW_MOVING, titleWin_windowMovingHandler);
			//this.preferenceWindow.closeButton.addEventListener(MouseEvent.CLICK, closingProjectWindow);
			this.preferenceWindow.addEventListener(CloseEvent.CLOSE, closeWindow);
		}
		
		/* Liste des Notifications ecouté par le mediator */
		override public function listNotificationInterests():Array {
			return [
				ProjectConstant.OPEN_PREFERENCEWINDOW,
			];
		}
		
		/* Gestion des Notifications */
		override public function handleNotification (notification:INotification):void {
			switch(notification.getName()) {
				case ProjectConstant.OPEN_PREFERENCEWINDOW :
					openPreferenceWindow();
					break;
			}
		}
		
		private function openPreferenceWindow():void {
			sendNotification(ShotConstant.CAPTURE_DISACTIVATE);
			PopUpManager.removePopUp(preferenceWindow);
			PopUpManager.addPopUp (this.preferenceWindow, this.app, true);
			PopUpManager.centerPopUp(preferenceWindow);
			if (!preferenceWindowInitialised) {
				var preferencesProxy:PreferencesProxy = facade.retrieveProxy(PreferencesProxy.NAME) as PreferencesProxy;
				preferenceWindow.init(preferencesProxy.workspaceDirectory);
				this.preferenceWindow.closeButton.addEventListener (MouseEvent.CLICK, closeWindow);
				preferenceWindowInitialised=true;
			}
		}
		
		private function workSpaceChanged(evt:ObjectEvent):void {
			sendNotification(ProjectConstant.PREFERENCE_WORKSPACE_CHANGED, evt.data);
		}
		
		public function setLanguage(language:String):void{
			this.preferenceWindow.setLanguage(language);
			sendNotification(ProjectConstant.LANGUAGE_SET);
		}
		
		private function languageChanged(evt:ObjectEvent):void {
			sendNotification(ProjectConstant.PREFERENCE_LANGUAGE_CHANGED, evt.data);
			sendNotification(ProjectConstant.LANGUAGE_SET);
		}
		
		private function windowResizing(evt:NativeWindowBoundsEvent):void{
			PopUpManager.centerPopUp(preferenceWindow);
		}
		
		protected function titleWin_windowMovingHandler(evt:TitleWindowBoundsEvent):void {				
			evt.stopImmediatePropagation();
			evt.preventDefault();
		}
		
		private function closeWindow(event:Event):void{
			sendNotification(ShotConstant.CAPTURE_ACTIVATE);
			PopUpManager.removePopUp(preferenceWindow);
		}
	}
}