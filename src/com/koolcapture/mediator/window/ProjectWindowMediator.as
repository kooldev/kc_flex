/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.mediator.window
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.PreferencesProxy;
	import com.koolcapture.model.ProjectProxy;
	import com.koolcapture.mxml.window.ProjectWindow;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NativeWindowBoundsEvent;
	import flash.filesystem.File;
	
	import mx.managers.PopUpManager;
	
	import spark.events.TitleWindowBoundsEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ProjectWindowMediator extends Mediator {
		public static const NAME:String="ProjectWindowMediator";
		private var app:KoolCapture;
		private var projectWindow:ProjectWindow;
		private var projectWindowInitialised:Boolean;
		
		public function ProjectWindowMediator(viewComponent:Object=null, mediatorName:String=NAME) {
			super(mediatorName, viewComponent);
			this.app = KoolCapture (viewComponent);
			this.app.addEventListener(NativeWindowBoundsEvent.RESIZING, windowResizing);
			this.projectWindow = KoolCapture (viewComponent).projectWindow;
			this.projectWindow.addEventListener(TitleWindowBoundsEvent.WINDOW_MOVING, windowMoving);
		}
		
		/* Liste des Notifications ecouté par le mediator */
		override public function listNotificationInterests ():Array {
			return [
				ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW_WITHOUT_CLOSE_BUTTON,
				ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW,
				ProjectConstant.LOAD_SHOT_SUCCESS,
				ProjectConstant.CREATE_PROJECT_SUCCESS,
				ProjectConstant.CREATE_SHOT_SUCCESS,
				ProjectConstant.PREFERENCES_LOADED
			];
		}
		
		/* Gestion des Notifications */
		override public function handleNotification (notification:INotification):void {
			switch (notification.getName()) {
				case ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW_WITHOUT_CLOSE_BUTTON :
					openWindow();
					projectWindow.closeButton.visible=false;
					break;
				case ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW :
					openWindow();
					projectWindow.closeButton.visible=true;
					break;
				case ProjectConstant.LOAD_SHOT_SUCCESS :
					closingProjectWindow(null);
					break;
				case ProjectConstant.CREATE_PROJECT_SUCCESS :
					createProjectSuccess();
					break;
				case ProjectConstant.CREATE_SHOT_SUCCESS :
					createShotSuccess();
					break;
				case ProjectConstant.PREFERENCES_LOADED :
					populateRecentShot();
					break;
				
			}
		}
		
		private function windowResizing (evt:NativeWindowBoundsEvent):void {
			PopUpManager.centerPopUp(projectWindow);
		}
		
		protected function windowMoving (evt:TitleWindowBoundsEvent):void {		
			evt.stopImmediatePropagation();
			evt.preventDefault();
		}
		
		private function openWindow ():void {
			PopUpManager.removePopUp(this.projectWindow);
			PopUpManager.addPopUp (this.projectWindow, this.app, true);
			PopUpManager.centerPopUp(projectWindow);
			if(!projectWindowInitialised) {
				projectWindow.init();
				this.projectWindow.addEventListener(ProjectWindow.CREATE_PROJECT, createProject);
				this.projectWindow.addEventListener(ProjectWindow.CREATE_OR_OPEN_SHOT, createOrOpenShot);
				this.projectWindow.addEventListener(ProjectWindow.EXPORT_PROJECT, exportProject);
				
				this.projectWindow.addEventListener(ProjectWindow.PROJECT_SELECTED, projectSelected);
				this.projectWindow.addEventListener(ProjectWindow.SHOT_SELECTED, shotSelected);
				
				this.projectWindow.closeButton.addEventListener(MouseEvent.CLICK, closingProjectWindow);
				projectWindowInitialised=true;
			}
			
			projectWindow.setWithWorkSpace(preferencesProxy.workspaceDirectory);
			this.populateRecentShot()
			projectProxy.modalWindowIsOpen=true;
		}
		
		private function projectSelected(evt:Event):void {
			diskPathsProxy.projectName=this.projectWindow.projectCreationInput.text;
			sendNotification(ProjectConstant.LOAD_PROJECT);
		}

		private function shotSelected(evt:Event):void {
			diskPathsProxy.shotName=this.projectWindow.shotCreationInput.text;
		}
		
		private function createProject(evt:Event):void {			
			sendNotification(ProjectConstant.CREATE_PROJECT, this.projectWindow.projectCreationInput.text);
		}
		
		private function createProjectSuccess():void{
			this.projectWindow.projectCreatedSuccessfully(diskPathsProxy.projectFolderPath);
		}

		private function createOrOpenShot(evt:Event):void{
			diskPathsProxy.shotName=this.projectWindow.shotCreationInput.text;
			sendNotification(ProjectConstant.CREATE_AND_LOAD_SHOT);
		}
		
		private function createShotSuccess():void{
			this.projectWindow.shotCreatedSuccessfully (diskPathsProxy.shotsFolderPath);
		}
		
		private function exportProject(evt:Event):void {
			sendNotification(ProjectConstant.EXPORT_PROJECT);
		}
		
		private function populateRecentShot():void{
			var projectName:String=preferencesProxy.recentFilm;
			var projectFile:File=new File(diskPathsProxy.workSpaceFolder + preferencesProxy.recentFilm);
			var shotName:String="";
			if (projectFile.exists && projectFile.isDirectory) {
				diskPathsProxy.projectName=preferencesProxy.recentFilm;
				shotName=preferencesProxy.recentShot;
				var shotFile:File=new File(diskPathsProxy.workSpaceFolder + preferencesProxy.recentFilm +"/"+preferencesProxy.recentShot)
				if(shotFile.exists && shotFile.isDirectory) {
					this.projectWindow.populateRecentShot(preferencesProxy.recentFilm, true, preferencesProxy.recentShot, true);
				} else {
					this.projectWindow.populateRecentShot(preferencesProxy.recentFilm, true, preferencesProxy.recentShot, false);
				}
			}else{
				this.projectWindow.populateRecentShot(preferencesProxy.recentFilm, false, preferencesProxy.recentShot, false);				
			}
		}
		
		private function closingProjectWindow (evt:MouseEvent):void{
			PopUpManager.removePopUp(projectWindow);
			projectProxy.modalWindowIsOpen=false;
		}
		
		public function get preferencesProxy ():PreferencesProxy {
			return facade.retrieveProxy(PreferencesProxy.NAME) as PreferencesProxy;;
		}
		
		public function get diskPathsProxy ():DiskPathsProxy {
			return facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
		}	
		
		public function get projectProxy ():ProjectProxy {
			return facade.retrieveProxy(ProjectProxy.NAME) as ProjectProxy;
		}
	}
}