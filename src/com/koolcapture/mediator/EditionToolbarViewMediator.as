/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.mediator {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.model.SoundProxy;
	import com.koolcapture.model.vo.SoundVO;
	import com.koolcapture.mxml.EditionToolBarView;
	
	import flash.events.Event;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class EditionToolbarViewMediator extends Mediator {
		public static const NAME:String	= "EditionToolbarVeiwMediator";
		
		public function EditionToolbarViewMediator(viewComponent:Object=null) {
			super(NAME, viewComponent);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_COPY, copy_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_CUT, cut_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_DELETE, delete_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_OPEN_PHOTO_BUCKET, open_photo_bucket_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_PASTE_BEFORE, paste_frame_before);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_PASTE_AFTER, paste_frame_after);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_REVERSE, reverse_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_REVERSEPASTE, reverse_paste_frame);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_EXPORT, export);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_IMPORT_SOUND, importSound);
			editionToolbarView.addEventListener(EditionToolBarView.EVENT_DELETE_SOUND, deleteSound);
		}
		
		override public function listNotificationInterests():Array {
			return [
				ShotConstant.FRAMES_COPIED,
				ShotConstant.REVERSABLE_FRAMES,
				ShotConstant.DELETABLE_FRAMES,
				ShotConstant.CUT_AND_COPYABLE,
				ShotConstant.PASTABLE_FRAMES,
				ShotConstant.HAS_SOUND
			];
		}
		
		/* Gestion des notifications de l'application */
		override public function handleNotification(note:INotification):void {
			switch(note.getName()) {
				case ShotConstant.HAS_SOUND:
					var hasSound:Boolean = note.getBody() as Boolean;
					this.hasSound(hasSound);
					break;
				case ShotConstant.FRAMES_COPIED:
					editionToolbarView.btnCut.enabled=true;
					editionToolbarView.btnPasteBefore.enabled=true;
					editionToolbarView.btnPasteAfter.enabled=true;
					editionToolbarView.btnPasteInverser.enabled=true;
					editionToolbarView.btnCut.alpha=1.0;
					editionToolbarView.btnPasteBefore.alpha=1.0;
					editionToolbarView.btnPasteAfter.alpha=1.0;
					editionToolbarView.btnPasteInverser.alpha=1.0;
					break;
				case ShotConstant.REVERSABLE_FRAMES:
					var reversable:Boolean = note.getBody() as Boolean;
					editionToolbarView.btnInverser.enabled=reversable;
					editionToolbarView.btnInverser.alpha=(reversable) ? 1.0 : 0.5;
					break;
				case ShotConstant.DELETABLE_FRAMES:
					var deletable:Boolean = note.getBody() as Boolean;
					editionToolbarView.btnSupprimer.enabled=deletable;
					editionToolbarView.btnSupprimer.alpha=(deletable) ? 1.0 : 0.5;
					break;
				case ShotConstant.PASTABLE_FRAMES:
					var pastable:Boolean = note.getBody() as Boolean;
					editionToolbarView.btnPasteAfter.enabled=pastable;
					editionToolbarView.btnPasteBefore.enabled=pastable;
					editionToolbarView.btnPasteInverser.enabled=pastable;
					editionToolbarView.btnPasteAfter.alpha=(pastable) ? 1.0 : 0.5;
					editionToolbarView.btnPasteBefore.alpha=(pastable) ? 1.0 : 0.5;
					editionToolbarView.btnPasteInverser.alpha=(pastable) ? 1.0 : 0.5;
					break;
				case ShotConstant.CUT_AND_COPYABLE:
					var copyAndPastable:Boolean = note.getBody() as Boolean;
					editionToolbarView.btnCopy.enabled=copyAndPastable;
					editionToolbarView.btnCut.enabled=copyAndPastable;
					editionToolbarView.btnCopy.alpha=(copyAndPastable) ? 1.0 : 0.5;
					editionToolbarView.btnCut.alpha=(copyAndPastable) ? 1.0 : 0.5;
					break;
			}
		}
		
		private function hasSound(hasSound:Boolean):void {
			var soundVO:SoundVO = soundProxy.soundVO;
			editionToolbarView.deleteSoundBtn.visible=editionToolbarView.soundNameLbl.visible=hasSound;
			editionToolbarView.import_button.alpha=(hasSound) ? 1.0 : 0.8;
			var shortName:String="";
			if(soundVO){
				shortName = soundVO.originalName.substring(0, soundVO.originalName.lastIndexOf("."));
			}
			editionToolbarView.soundNameLbl.text=(hasSound) ? shortName : "";
		}
		
		private function copy_frame(evt:Event):void{
			sendNotification(ShotConstant.COPY_FRAMES);
		}
		
		private function cut_frame(evt:Event):void{
			sendNotification(ShotConstant.CUT_FRAMES);
		}
		
		private function delete_frame(evt:Event):void{
			sendNotification(ShotConstant.DELETE_FRAMES);
		}
		
		private function open_photo_bucket_frame(evt:Event):void{
			sendNotification( ProjectConstant.OPEN_PHOTO_BUCKET_WINDOW);
		}
		
		private function paste_frame_before(evt:Event):void{
			sendNotification(ShotConstant.PASTE_FRAMES_BEFORE);
		}
		
		private function paste_frame_after(evt:Event):void{
			sendNotification(ShotConstant.PASTE_FRAMES_AFTER);
		}
		
		private function reverse_frame(evt:Event):void{
			sendNotification(ShotConstant.REVERSE_FRAMES);
		}
		
		private function reverse_paste_frame(evt:Event):void{
			sendNotification(ShotConstant.PASTE_INVERTED_FRAMES);
		}
		
		private function export(evt:Event):void{
			sendNotification(ProjectConstant.OPEN_EXPORT_WINDOW);
		}

		public function importSound(evt:Event):void {
			sendNotification(ShotConstant.IMPORT_SOUND); 
		}

		public function deleteSound(evt:Event):void {
			sendNotification(ShotConstant.DELETE_SOUND); 
		}
		
		public function get editionToolbarView():EditionToolBarView { return viewComponent as  EditionToolBarView; }
		public function get soundProxy ():SoundProxy { return facade.retrieveProxy (SoundProxy.NAME) as SoundProxy; }
	}
}