/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.mediator {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.event.ObjectEvent;
	import com.koolcapture.mxml.CameraToolbarView;
	import flash.events.Event;
	import flash.media.Camera;
	import flash.media.scanHardware;
	import mx.collections.ArrayCollection;
	import spark.components.DropDownList;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class CameraToolbarViewMediator extends Mediator {
		public static const NAME:String	= "CameraToolbarViewMedaitor";
		private var scanning:Boolean=false;
		private var cameraArrayCollection:ArrayCollection;
		
		public function CameraToolbarViewMediator(viewComponent:Object=null) {
			super(NAME, viewComponent);
			cameraToolbarView.addEventListener(CameraToolbarView.EVENT_CAMERA_CHANGED, cameraChanged);
			cameraToolbarView.addEventListener(CameraToolbarView.EVENT_REFRESH_CAMERA_LIST, refreshCameraList);
			setUpCameraInfo ();
		}
		
		/* Liste des Notifications ecouté par le mediator */
		override public function listNotificationInterests():Array {
			return [
				ProjectConstant.LANGUAGE_SET
			];
		}
		
		/* Gestion des Notifications */
		override public function handleNotification (notification:INotification):void {
			switch(notification.getName()) {
				case ProjectConstant.LANGUAGE_SET :
					var cameraDeviceNames:Array=Camera.names;
					if (cameraDeviceNames.length==0) {
						cameraToolbarView.cameraComboBox.enabled=true;
						cameraArrayCollection=new ArrayCollection();
						cameraArrayCollection.addItemAt(cameraToolbarView.noCameraConnectedText, 0);
						cameraToolbarView.cameraComboBox.dataProvider = cameraArrayCollection;
						cameraToolbarView.cameraComboBox.selectedIndex=0;
					}
					break;
			}
		}
		
		private function refreshCameraList(evt:Event):void{
			sendNotification(ProjectConstant.CHANGE_CAMERA, -1);
			scanHardware();
			setUpCameraInfo();
		}
		
		private function setUpCameraInfo ():void {
			cameraArrayCollection=new ArrayCollection();
			
			var cameraComboBox:DropDownList=cameraToolbarView.cameraComboBox;
			cameraComboBox.dataProvider = cameraArrayCollection;
			var cameraDeviceNames:Array=Camera.names;
			if (cameraDeviceNames.length==0) {
				cameraComboBox.enabled=false;

				cameraArrayCollection.addItem(cameraToolbarView.noCameraConnectedText);
				cameraComboBox.selectedIndex=0;
				cameraToolbarView.cameraComboBox.enabled=false;
			} else {
				for (var i:int=0; i<cameraDeviceNames.length; i++) {
					var camera:String=cameraDeviceNames[i];
					cameraArrayCollection.addItem(camera)
				}
				cameraComboBox.selectedIndex=0;
				cameraToolbarView.cameraComboBox.enabled=true;
			}
			sendNotification (ProjectConstant.CHANGE_CAMERA, 0);
		}
		
		public function cameraChanged(evt:ObjectEvent):void {
			sendNotification(ProjectConstant.CHANGE_CAMERA, evt.data.cameraNumber);
		}
		
		public function get cameraToolbarView():CameraToolbarView { return viewComponent as CameraToolbarView; }
	}
}