/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.setup.project {
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.HistoryProxy;
	import com.koolcapture.model.NativeProcessProxy;
	import com.koolcapture.model.ProjectProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SetupProjectProxies extends SimpleCommand {
		override public function execute(notification:INotification):void{
			facade.registerProxy(new ProjectProxy());
			facade.registerProxy(new HistoryProxy());
			facade.registerProxy(new CameraProxy());
			facade.registerProxy(new NativeProcessProxy());
		}
	}
}