/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.setup.project
{
	import com.koolcapture.command.project.ActiveCameraCmd;
	import com.koolcapture.command.project.ChangeCameraCmd;
	import com.koolcapture.command.project.CheckForUpDatesCmd;
	import com.koolcapture.command.project.CreateProjectCmd;
	import com.koolcapture.command.project.CreateShotCmd;
	import com.koolcapture.command.project.ExportProjectCmd;
	import com.koolcapture.command.project.FullScreenCmd;
	import com.koolcapture.command.project.FullScreenLeftCmd;
	import com.koolcapture.command.project.HistoryAddCmd;
	import com.koolcapture.command.project.HistoryRedoCmd;
	import com.koolcapture.command.project.HistoryUndoCmd;
	import com.koolcapture.command.project.LoadProjectCmd;
	import com.koolcapture.command.project.LoadShotCmd;
	import com.koolcapture.command.project.LoadShotSuccess;
	import com.koolcapture.command.project.SaveShotCmd;
	import com.koolcapture.command.project.ToggleAutomaticUpdateCmd;
	import com.koolcapture.command.project.ToggleFullscreenCmd;
	import com.koolcapture.command.shot.ExportFilmCmd;
	import com.koolcapture.command.shot.ExportImagesCmd;
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SetupProjectCommands extends SimpleCommand {
			override public function execute(notification:INotification):void {
				facade.registerCommand(ProjectConstant.CREATE_PROJECT, CreateProjectCmd);
				facade.registerCommand(ProjectConstant.LOAD_PROJECT, LoadProjectCmd);
				facade.registerCommand(ProjectConstant.CREATE_AND_LOAD_SHOT, CreateShotCmd);
				
				facade.registerCommand(ProjectConstant.LOAD_SHOT, LoadShotCmd);
				facade.registerCommand(ProjectConstant.LOAD_SHOT_SUCCESS, LoadShotSuccess);
				facade.registerCommand(ProjectConstant.SAVE_SHOT, SaveShotCmd);
				facade.registerCommand(ShotConstant.EXPORT_TAKE_IMAGES, ExportImagesCmd);
				facade.registerCommand(ShotConstant.EXPORT_FILM, ExportFilmCmd);
				facade.registerCommand(ProjectConstant.FULLSCREEN, FullScreenCmd);
				facade.registerCommand(ProjectConstant.FULLSCREEN_LEFT, FullScreenLeftCmd);
				facade.registerCommand(ProjectConstant.TOGGLE_FULLSCREEN, ToggleFullscreenCmd);
				
				facade.registerCommand(ProjectConstant.ADD_HISTORY, HistoryAddCmd);
				facade.registerCommand(ProjectConstant.UNDO_HISTORY, HistoryUndoCmd);
				facade.registerCommand(ProjectConstant.REDO_HISTORY, HistoryRedoCmd);
				facade.registerCommand(ProjectConstant.CHECK_FOR_UPDATES, CheckForUpDatesCmd);
				
				facade.registerCommand(ProjectConstant.TOGGLE_AUTOMATIC_UPDATES, ToggleAutomaticUpdateCmd);
				facade.registerCommand(ProjectConstant.CHANGE_CAMERA, ChangeCameraCmd);
				facade.registerCommand(ProjectConstant.ACTIVATE_CAMERA, ActiveCameraCmd);
				facade.registerCommand(ProjectConstant.EXPORT_PROJECT, ExportProjectCmd);
			}
	}
}