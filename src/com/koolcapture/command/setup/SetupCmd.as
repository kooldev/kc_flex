/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.setup {
	import com.koolcapture.command.setup.disk.SetupDiskProxy;
	import com.koolcapture.command.setup.mainwindow.SetupMainWindowMediatorCmd;
	import com.koolcapture.command.setup.nativemenu.SetupNativeCommands;
	import com.koolcapture.command.setup.photobucket.SetupPhotoBucketWindowMediator;
	import com.koolcapture.command.setup.preference.SetupPreferenceCommands;
	import com.koolcapture.command.setup.preference.SetupPreferenceProxy;
	import com.koolcapture.command.setup.preference.SetupPreferenceWindowMediator;
	import com.koolcapture.command.setup.project.SetupProjectCommands;
	import com.koolcapture.command.setup.project.SetupProjectMediators;
	import com.koolcapture.command.setup.project.SetupProjectProxies;
	import com.koolcapture.command.setup.shot.SetupShotCommands;
	import com.koolcapture.command.setup.shot.SetupShotMediators;
	import com.koolcapture.command.setup.shot.SetupShotProxies;
	import com.koolcapture.command.setup.timelapse.SetupTimelapseWindowMediator;
	
	import org.puremvc.as3.patterns.command.MacroCommand;
	
	public class SetupCmd extends MacroCommand {
		override protected function initializeMacroCommand():void {
			addSubCommand(SetupMainWindowMediatorCmd);
			
			addSubCommand(SetupProjectCommands);
			addSubCommand(SetupProjectMediators);
			addSubCommand(SetupProjectProxies);
			
			addSubCommand(SetupPreferenceWindowMediator);
			addSubCommand(SetupPreferenceProxy);
			addSubCommand(SetupPreferenceCommands);
			
			addSubCommand(SetupDiskProxy);
			addSubCommand(SetupShotProxies);
			addSubCommand(SetupShotMediators);
			addSubCommand(SetupShotCommands);
			
			addSubCommand(SetupNativeCommands);
			
			addSubCommand(SetupTimelapseWindowMediator);
			
			addSubCommand(SetupPhotoBucketWindowMediator);
		}
	}
}