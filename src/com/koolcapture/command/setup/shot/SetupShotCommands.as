/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.setup.shot {
	import com.koolcapture.command.shot.CurrentFrameChangedCmd;
	import com.koolcapture.command.shot.DeleteSoundCmd;
	import com.koolcapture.command.shot.GotoFirstFrameCmd;
	import com.koolcapture.command.shot.GotoFrameCmd;
	import com.koolcapture.command.shot.GotoLastFrameCmd;
	import com.koolcapture.command.shot.GotoNextFrameCmd;
	import com.koolcapture.command.shot.GotoPrevFrameCmd;
	import com.koolcapture.command.shot.GotoSecondLastFrameCmd;
	import com.koolcapture.command.shot.LiveVideoHiddenCmd;
	import com.koolcapture.command.shot.ImportSoundCmd;
	import com.koolcapture.command.shot.OnionSkinAlphaChangedCmd;
	import com.koolcapture.command.shot.SaveShotXMLToDisk;
	import com.koolcapture.command.shot.SelectAllFramesCmd;
	import com.koolcapture.command.shot.SelectNextFrameCmd;
	import com.koolcapture.command.shot.SelectPrevFrameCmd;
	import com.koolcapture.command.shot.SetCaptureNumberCmd;
	import com.koolcapture.command.shot.SetFramePerSecCmd;
	import com.koolcapture.command.shot.SetPlayBackQualityCmd;
	import com.koolcapture.command.shot.LiveVideoShownCmd;
	import com.koolcapture.command.shot.StopCmd;
	import com.koolcapture.command.shot.SwitchLoopCmd;
	import com.koolcapture.command.shot.SwitchPlayCmd;
	import com.koolcapture.command.shot.TaketimelineMediatorFrameChangedCmd;
	import com.koolcapture.command.shot.TimelineZoomInCmd;
	import com.koolcapture.command.shot.TimelineZoomOutCmd;
	import com.koolcapture.command.shot.ToggleGridCmd;
	import com.koolcapture.command.shot.ToggleHorizontalFlipCmd;
	import com.koolcapture.command.shot.ToggleLiveVideoCmd;
	import com.koolcapture.command.shot.ToggleShortplayCmd;
	import com.koolcapture.command.shot.ToggleVerticalFlipCmd;
	import com.koolcapture.command.shot.ToggleliveViewState;
	import com.koolcapture.command.shot.edition.CaptureFrameCmd;
	import com.koolcapture.command.shot.edition.CopyFramesCmd;
	import com.koolcapture.command.shot.edition.CutFramesCmd;
	import com.koolcapture.command.shot.edition.DeleteFramesCmd;
	import com.koolcapture.command.shot.edition.DuplicateFramesCmd;
	import com.koolcapture.command.shot.edition.ImportImageCmd;
	import com.koolcapture.command.shot.edition.InsertPhotoBucketFramesCmd;
	import com.koolcapture.command.shot.edition.InvertedPasteFramesCmd;
	import com.koolcapture.command.shot.edition.PasteFramesAfterCmd;
	import com.koolcapture.command.shot.edition.PasteFramesBeforeCmd;
	import com.koolcapture.command.shot.edition.ReverseFramesCmd;
	import com.koolcapture.command.shot.edition.SetEditableStateCmd;
	import com.koolcapture.constant.ShotConstant;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SetupShotCommands extends SimpleCommand {
		override public function execute(notification:INotification):void {
			facade.registerCommand(ShotConstant.SET_EDITIBLE_STATE, SetEditableStateCmd);
			facade.registerCommand(ShotConstant.GOTO_FIRST_FRAME, GotoFirstFrameCmd);
			facade.registerCommand(ShotConstant.GOTO_LAST_FRAME, GotoLastFrameCmd);
			facade.registerCommand(ShotConstant.GOTO_SECOND_LAST_FRAME, GotoSecondLastFrameCmd);
			facade.registerCommand(ShotConstant.GOTO_FRAME, GotoFrameCmd);
			facade.registerCommand(ShotConstant.GOTO_NEXT_FRAME, GotoNextFrameCmd);
			facade.registerCommand(ShotConstant.GOTO_PREV_FRAME, GotoPrevFrameCmd);
			facade.registerCommand(ShotConstant.TRANSPORT_SWITCH_PLAY, SwitchPlayCmd);
			facade.registerCommand(ShotConstant.TRANSPORT_SWITCH_LOOP, SwitchLoopCmd);
			facade.registerCommand(ShotConstant.TRANSPORT_STOP, StopCmd);
			facade.registerCommand(ShotConstant.TRANSPORT_SET_FPS, SetFramePerSecCmd);
			facade.registerCommand(ShotConstant.TRANSPORT_SET_PLAYBACK_QUALITY, SetPlayBackQualityCmd);
			facade.registerCommand(ShotConstant.CURRENT_FRAME_CHANGED, CurrentFrameChangedCmd);
			facade.registerCommand(ShotConstant.COPY_FRAMES, CopyFramesCmd);
			facade.registerCommand(ShotConstant.CUT_FRAMES, CutFramesCmd);
			facade.registerCommand(ShotConstant.DELETE_FRAMES, DeleteFramesCmd);
			facade.registerCommand(ShotConstant.PASTE_FRAMES_BEFORE, PasteFramesBeforeCmd);
			facade.registerCommand(ShotConstant.PASTE_FRAMES_AFTER, PasteFramesAfterCmd);
			facade.registerCommand(ShotConstant.REVERSE_FRAMES, ReverseFramesCmd);
			facade.registerCommand(ShotConstant.PASTE_INVERTED_FRAMES, InvertedPasteFramesCmd);
			facade.registerCommand(ShotConstant.SELECT_ALL_FRAMES, SelectAllFramesCmd);
			facade.registerCommand(ShotConstant.DUPLICATE_FRAMES, DuplicateFramesCmd);
			facade.registerCommand(ShotConstant.TOGGLE_LIVE_VIDEO, ToggleLiveVideoCmd);
			facade.registerCommand(ShotConstant.TOGGLE_GRID, ToggleGridCmd);
			facade.registerCommand(ShotConstant.TOGGLE_VERTICAL_FLIP, ToggleVerticalFlipCmd);
			facade.registerCommand(ShotConstant.TOGGLE_HORIZONTAL_FLIP, ToggleHorizontalFlipCmd);
			facade.registerCommand(ShotConstant.TIMELINE_ZOOM_IN, TimelineZoomInCmd);
			facade.registerCommand(ShotConstant.TIMELINE_ZOOM_OUT, TimelineZoomOutCmd);
			facade.registerCommand(ShotConstant.LIVE_VIDEO_SHOWN, LiveVideoShownCmd);
			facade.registerCommand(ShotConstant.LIVE_VIDEO_HIDDEN, LiveVideoHiddenCmd);
			facade.registerCommand(ShotConstant.TAKETIMELINEMEDIATOR_FRAME_CHANGED, TaketimelineMediatorFrameChangedCmd);
			facade.registerCommand(ShotConstant.CAPTURE_FRAMES, CaptureFrameCmd);
			facade.registerCommand(ShotConstant.INSERT_PHOTO_BUCKET_FRAMES, InsertPhotoBucketFramesCmd);
			facade.registerCommand(ShotConstant.ONIONSKIN_ALPHA_VALUE_CHANGE, OnionSkinAlphaChangedCmd);
			facade.registerCommand(ShotConstant.SELECT_NEXT_FRAME, SelectNextFrameCmd);
			facade.registerCommand(ShotConstant.SELECT_PREVIOUS_FRAME, SelectPrevFrameCmd);
			facade.registerCommand(ShotConstant.IMPORT_IMAGE, ImportImageCmd);
			facade.registerCommand(ShotConstant.IMPORT_SOUND, ImportSoundCmd);
			facade.registerCommand(ShotConstant.DELETE_SOUND, DeleteSoundCmd);
			facade.registerCommand(ShotConstant.TOGGLE_LIVE_VIEW_STATE, ToggleliveViewState);
			facade.registerCommand(ShotConstant.TOGGLE_SHORTPLAY, ToggleShortplayCmd);
			facade.registerCommand(ShotConstant.CAPTURE_NUMBER_CHANGED, SetCaptureNumberCmd);
			facade.registerCommand(ShotConstant.SAVE_DATA_TO_DISK, SaveShotXMLToDisk);
		}
	}
}