package com.koolcapture.command.shot
{
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.model.SoundProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

	public class DeleteSoundCmd extends SimpleCommand {
		
		override public function execute(notification:INotification):void {
			soundProxy.removeSound(soundProxy.soundVO);
			sendNotification(ShotConstant.HAS_SOUND, false);
		}
		
		public function get soundProxy ():SoundProxy { return facade.retrieveProxy (SoundProxy.NAME) as SoundProxy; }
	}
}