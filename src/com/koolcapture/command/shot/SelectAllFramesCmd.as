/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot
{
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SelectAllFramesCmd extends SimpleCommand
	{
		override public function execute(notification:INotification):void
		{
			var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
			var timeLineProxy:ShotTimeLineProxy= facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			
			var selectList:Vector.<int> = new Vector.<int>();
			var lastFrameToSelect:int=timeLineProxy.numberFrames;
			if(cameraProxy.cameraIsActive){
				lastFrameToSelect=timeLineProxy.numberFrames-1;
			}
			
			for (var i:int=0; i<lastFrameToSelect; i++){
				selectList.push(i);
			}
				
			timeLineMediator.selectedIndices=selectList;
			if (cameraProxy.cameraIsActive) {
				var wishIndex:int= (timeLineProxy.numberFrames - 2>=0) ? 2 : 1;
				sendNotification(ShotConstant.GOTO_FRAME, wishIndex);
			} else {
				sendNotification(ShotConstant.GOTO_FRAME, timeLineProxy.numberFrames - 1);
			}
			sendNotification(ShotConstant.SET_EDITIBLE_STATE, ShotConstant.EDITIBLE_STATE_FORCE_EDITABLE);
			sendNotification(ShotConstant.SET_EDITIBLE_STATE, ShotConstant.EDITIBLE_STATE_FORCE_REVERSABLE);
			
		}
		
		public function get cameraProxy ():CameraProxy { return facade.retrieveProxy (CameraProxy.NAME) as CameraProxy; }
	}
}