/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot {
	import com.koolcapture.AppFacade;
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.NativeProcessProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.SoundProxy;
	import com.koolcapture.model.vo.FrameVO;
	import com.koolcapture.mxml.window.ExportationWindow;
	import com.koolcapture.tools.Primitive;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ExportFilmCmd extends SimpleCommand {
		private var exportationInProgressWindow:ExportationWindow;
		private var resourceManager:IResourceManager;
		private var exportImageIndex:int=0;
		private var takeTimeLineProxy:ShotTimeLineProxy;
		private var fileBaseName:String;
		private var imageDirectory:File;
		private var videoDirectory:File;
		private var exportTimer:Timer=new Timer(1,0);
		private var timer:Timer;
		
		override public function execute(notification:INotification):void {
			fileBaseName = notification.getBody() as String;
			if (!fileBaseName){
				var diskPathProxy:DiskPathsProxy= facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
				fileBaseName = diskPathProxy.projectName+"_"+diskPathProxy.shotName;
			}
			
			this.resourceManager= AppFacade.getInstance().resourceManager;
			var dialog:File= File.desktopDirectory;
			dialog.addEventListener(Event.SELECT, directorySelected);
			dialog.addEventListener(Event.CANCEL, canceled);
			
			openPopup();
			dialog.browseForDirectory(resourceManager.getString('GUI_I18NS'
				,'choose_a_folder'));
		}
		
		private function openPopup():void {
			exportationInProgressWindow = new ExportationWindow();
			exportationInProgressWindow.width = 250;
			exportationInProgressWindow.height = 150;
			exportationInProgressWindow.title = resourceManager.getString('GUI_I18NS', 'export_in_progress');
			exportationInProgressWindow.setStyle("modalTrasparancy",0.2);
			exportationInProgressWindow.setStyle("modalTransparencyBlur",5);
			exportationInProgressWindow.setStyle("modalTransparencyDuration",1500);
			
			PopUpManager.addPopUp(exportationInProgressWindow,AppFacade.getInstance().app,true);
			PopUpManager.centerPopUp(exportationInProgressWindow);
		}
		
		private function removePopup():void {
			PopUpManager.removePopUp(exportationInProgressWindow);
		}
		
		private function canceled(event:Event):void{
			removePopup();
		}
		
		private function directorySelected(event:Event):void  {
			var directory:File = event.target as File;
			videoDirectory=new File(directory.url);
			videoDirectory.createDirectory();
			imageDirectory= new File(directory.url+"/"+fileBaseName);
			imageDirectory.createDirectory();
			takeTimeLineProxy= facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			
			startExporting();
		}
		
		private function startExporting():void {
			exportImage(0);
			exportTimer.addEventListener(TimerEvent.TIMER, exportTick);	
			exportTimer.start();	
		}
		
		private function exportTick(evt:TimerEvent):void {
			exportImageIndex++;
			var endFrame:int=takeTimeLineProxy.frames.length;
			if( cameraProxy.cameraIsActive){
				endFrame=takeTimeLineProxy.frames.length-1;
			}
			if(exportImageIndex<endFrame){
				exportImage(exportImageIndex);
			}else{
				exportTimer.stop();
				exportVideo();
			}
		}
		
		private function exportImage(index:int):void{
			var frameVO:FrameVO=FrameVO(takeTimeLineProxy.frames.getItemAt(index));
			var viewByteArray:ByteArray=frameVO.viewByteArray;
			
			var i:int=index+1;
			exportationInProgressWindow.exportProgressBar.setProgress(i/takeTimeLineProxy.frames.length*100/2, 100);
			exportationInProgressWindow.exportProgressBar.label=resourceManager.getString('GUI_I18NS'
				,'exported')+" "+ int((i/takeTimeLineProxy.frames.length*100)/2)+"%";
			var photoFile:File = new File(imageDirectory.nativePath+"/"+fileBaseName+getDigits(i, 5)+".png");
			
			var photoStream:FileStream = new FileStream();
			if (viewByteArray) {
				photoStream.open(photoFile, FileMode.WRITE);
				photoStream.writeBytes(viewByteArray, 0, viewByteArray.length);
				photoStream.close();
			} else {
				var blankFile:File = new File ("app:/assets/blank1280x720.png");
				if (blankFile.exists){
					blankFile.copyToAsync(photoFile, true);
				}
			}
		}

		private var process:NativeProcess;
		private var finalProcess:NativeProcess;
		private var bytes:ByteArray = new ByteArray();
		private var resultExportVideo:String="";
		private var progressValue:Number=0;
		private var _soundVideoURL:String;
		
		private function exportVideo():void {
			var imagePath:String;
			if ( Primitive.os() == Primitive.MAC ) {
				imageDirectory.nativePath +"/"+fileBaseName+ "%05d.png";
			}else{
				imageDirectory.nativePath +"\\"+fileBaseName+ "%05d.png";
			}
				
			var _nativeProcessStartupInfo:NativeProcessStartupInfo=nativeProcessProxy.ffmpegProcessStartupInfo();
						
			var processArgs:Vector.<String> = new Vector.<String>();
			processArgs.push("-framerate");
			
			processArgs.push(String(timeLineProxy.fps));
			processArgs.push("-i");
			processArgs.push(imageDirectory.nativePath +"/"+fileBaseName+ "%05d.png");
			
			processArgs.push("-c:v"); 
			processArgs.push("libx264"); 
			processArgs.push("-pix_fmt"); 
			processArgs.push("yuv420p"); 
			
			var videoFile:File = videoDirectory.resolvePath( fileBaseName+ "_tmp.mp4");
			if (videoFile.exists && ! videoFile.isDirectory) { videoFile.deleteFile(); }
			
			var videoURL:String = videoFile.nativePath;
			processArgs.push(videoURL); 
			
			_nativeProcessStartupInfo.arguments = processArgs;
			_nativeProcessStartupInfo.workingDirectory = File.documentsDirectory; 
			
			process = new NativeProcess();
			//process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, errorDataHandler); 
			//process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, dataHandler); 
			process.addEventListener(NativeProcessExitEvent.EXIT, onExit);
			process.start(_nativeProcessStartupInfo);
			this.timer=new Timer(100,0);
			this.timer.addEventListener(TimerEvent.TIMER, this.percentageTimer);
			this.timer.start()
		}
		
		private function percentageTimer(timerEvent:TimerEvent):void{
			var fakeExportValue:int= Math.min(this.timer.currentCount*100, 8000);
			
			var fakePercentage:int=int(((fakeExportValue/10000)*50)+50);
			exportationInProgressWindow.exportProgressBar.setProgress(fakePercentage, 100);
			exportationInProgressWindow.exportProgressBar.label=resourceManager.getString('GUI_I18NS'
				,'exported')+" "+ fakePercentage+"%";
		}
		
		private function onExit(e:NativeProcessExitEvent):void {
			process.exit();
			imageDirectory.deleteDirectory(true);
			this.createVideoWithSound();
		}
		
		
		private function createVideoWithSound():void {
			var _nativeProcessStartupInfo:NativeProcessStartupInfo=nativeProcessProxy.ffmpegProcessStartupInfo();
			var processArgs:Vector.<String> = new Vector.<String>();
			var videoFile:File = videoDirectory.resolvePath(fileBaseName + "_tmp.mp4");
			if (!soundProxy.soundVO) {
				this.renameVideoWithOutSound();
				return;
			}
			
			var soundFile:File =  new File(diskPathsProxy.soundFolderPath+"/"+soundProxy.soundVO.originalName);
			if (!soundFile.exists) {
				this.renameVideoWithOutSound();
				return;
			}
			processArgs.push("-i");
			processArgs.push(soundFile.nativePath);
			
			processArgs.push("-i");
			var videoURL:String = videoFile.nativePath;
			processArgs.push(videoURL);
			
			var videoOutputFile:File = videoDirectory.resolvePath( fileBaseName+ ".mp4");
			if (videoOutputFile.exists && ! videoOutputFile.isDirectory) { videoOutputFile.deleteFile(); }
			processArgs.push(videoOutputFile.nativePath); 
			
			_nativeProcessStartupInfo.arguments = processArgs;
			_nativeProcessStartupInfo.workingDirectory = File.documentsDirectory; 
			
			finalProcess = new NativeProcess();
			//finalProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, finalErrorHandler); 
			//finalProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, finalVideoDataHandler); 
			finalProcess.addEventListener(NativeProcessExitEvent.EXIT, onFinalExit);
			finalProcess.start(_nativeProcessStartupInfo);
		}
		
		private function renameVideoWithOutSound():void{
			var videoFile:File = videoDirectory.resolvePath(fileBaseName + "_tmp.mp4");
			var finishedVideoFile:File = videoDirectory.resolvePath(fileBaseName + ".mp4");
			videoFile.moveTo(finishedVideoFile, true);
			exportationInProgressWindow.exportProgressBar.setProgress(100, 100);
			exportationInProgressWindow.exportProgressBar.label=resourceManager.getString('GUI_I18NS'
				,'cleanup');
			removePopup();
		}
		
		private function onFinalExit(e:NativeProcessExitEvent):void {
			this.timer.stop();
			finalProcess.exit();
			exportationInProgressWindow.exportProgressBar.setProgress(100, 100);
			exportationInProgressWindow.exportProgressBar.label=resourceManager.getString('GUI_I18NS'
				,'cleanup');
			var videoFile:File = videoDirectory.resolvePath( fileBaseName+ "_tmp.mp4");
			if (videoFile.exists && ! videoFile.isDirectory) { videoFile.deleteFile(); }
			removePopup();
		}
		
		private function getDigits(num:int, numdigits:int):String {
			var digitlength:int= numdigits-String(num).length;
			var zeroStr:String="";
			for(var i:int=0; i<digitlength; i++){
				zeroStr+="0"
			}
			return zeroStr+num;	
		}
		
		public function get diskPathsProxy ():DiskPathsProxy { return facade.retrieveProxy (DiskPathsProxy.NAME) as DiskPathsProxy; }
		public function get soundProxy ():SoundProxy { return facade.retrieveProxy (SoundProxy.NAME) as SoundProxy; }
		public function get timeLineProxy ():ShotTimeLineProxy { return facade.retrieveProxy (ShotTimeLineProxy.NAME) as ShotTimeLineProxy; }
		public function get cameraProxy ():CameraProxy { return facade.retrieveProxy (CameraProxy.NAME) as CameraProxy; }
		public function get nativeProcessProxy ():NativeProcessProxy { return facade.retrieveProxy (NativeProcessProxy.NAME) as NativeProcessProxy; }
		
	}
}

