/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.MonitorMediator;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.TimelineEditionProxy;
	import com.koolcapture.model.vo.FrameVO;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ToggleLiveVideoCmd extends SimpleCommand {
		override public function execute(notification:INotification):void {
			var timeLineProxy:ShotTimeLineProxy=facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			
			var monitorMediator:MonitorMediator = facade.retrieveMediator(MonitorMediator.NAME) as MonitorMediator;
			if (monitorMediator.isLiveVisible) {
				sendNotification(ShotConstant.GOTO_SECOND_LAST_FRAME);
			} else {
				timeLineProxy.stop();
				sendNotification(ShotConstant.GOTO_LAST_FRAME);
			}
			
			if (isCurrentFrameLiveView()) {
				deselectAll();				
			} else {
				setUpSelection();
			}
		}
	
		private function deselectAll():void {
			timelineEditionProxy.framesReversable=false;
			timelineEditionProxy.framesDeletable=false;
			timelineEditionProxy.framesCutAndCopyable=false;
			timelineEditionProxy.framesPastable=false;
		}
		
		private function setUpSelection():void {
			if (shotTimelineMediator.selectedItems && shotTimelineMediator.selectedItems.length>1) {
				timelineEditionProxy.framesReversable=true;
			} else {
				timelineEditionProxy.framesReversable=false;
			}
			
			timelineEditionProxy.framesCutAndCopyable=true;
			timelineEditionProxy.framesDeletable=true;
			timelineEditionProxy.framesPastable = timelineEditionProxy.hasCopiedFrames;
		}
			
		private function isCurrentFrameLiveView():Boolean {
			var currentFrameVO:FrameVO = FrameVO(shotTimeLineProxy.frames.getItemAt(shotTimeLineProxy.currentIndex));
			return (currentFrameVO.id == ProjectConstant.LIVE_VIEW_FRAMEVO); 
		}
		
		public function get shotTimeLineProxy():ShotTimeLineProxy {
			return facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;;
		}
		
		public function get shotTimelineMediator():ShotTimelineMediator {
			return facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;;
		}
		
		public function get timelineEditionProxy():TimelineEditionProxy {
			return facade.retrieveProxy(TimelineEditionProxy.NAME) as TimelineEditionProxy;;
		}
	}
}