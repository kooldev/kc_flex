/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot {
	import com.koolcapture.AppFacade;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.NativeProcessProxy;
	import com.koolcapture.model.SoundProxy;
	import com.koolcapture.mxml.window.ExportationWindow;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ImportSoundCmd extends SimpleCommand {
		private var file:File;
		private var finalProcess:NativeProcess;
		private var soundImportExtension:String;
		private var exportationInProgressWindow:ExportationWindow;
		private var resourceManager:IResourceManager;
		private var ticks:int=0;
		private var originalFile:File;
		override public function execute(notification:INotification):void {
			this.resourceManager= AppFacade.getInstance().resourceManager;
			soundImportExtension=".mp3";
			file= new File();
			file.addEventListener(Event.SELECT, onFileSelected);
			var resourceManager:IResourceManager= AppFacade.getInstance().resourceManager;
			file.browse([new FileFilter(resourceManager.getString('GUI_I18NS', "Sound"), "*.mp3;*.wav;*.ogg;*.aiff;")]);
			this.finalProcess = new NativeProcess();
			
		}
		
		private function onFileSelected(event:Event):void {
			openPopup();
			var _nativeProcessStartupInfo:NativeProcessStartupInfo=nativeProcessProxy.ffmpegProcessStartupInfo();
			
			var processArgs:Vector.<String> = new Vector.<String>();
			originalFile = File(event.target);
				
			var soundFolder:File=new File(diskPathsProxy.soundFolderPath)
			if (!soundFolder.exists) {
				soundFolder.createDirectory();
			}
			
			var newPath:String = diskPathsProxy.soundFolderPath+"/"+file.name ;
			var newFile:File=new File(newPath);
			if (newFile.exists) {
				newFile.deleteFile();
			}
			
			var fileWithExtension:File = new File(newPath + soundImportExtension); 
			if (fileWithExtension.exists) {
				fileWithExtension.deleteFile();
			}
			
			processArgs.push("-i");
			processArgs.push(originalFile.nativePath);
			processArgs.push("-ar");
			processArgs.push("44100");

			processArgs.push(newFile.nativePath + soundImportExtension);	
			originalFile.copyTo(newFile, true);
			
			
			
			_nativeProcessStartupInfo.arguments = processArgs;
			_nativeProcessStartupInfo.workingDirectory = File.documentsDirectory;
			
			finalProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, finalErrorHandler); 
			finalProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, finalDataHandler); 
			finalProcess.addEventListener(NativeProcessExitEvent.EXIT, onFinalExit);
			finalProcess.start(_nativeProcessStartupInfo);
		}
		
		private function finalDataHandler(event:ProgressEvent):void {			
		}
		
		private function finalErrorHandler(event:ProgressEvent):void {
			ticks=Math.min(ticks+3, 100);
			exportationInProgressWindow.exportProgressBar.setProgress(ticks, 100);
			exportationInProgressWindow.exportProgressBar.label = ticks+"%";
		}
		
		private function onFinalExit(e:NativeProcessExitEvent):void {
			soundProxy.addSound(originalFile.name + soundImportExtension);
			exportationInProgressWindow.exportProgressBar.setProgress(100, 100);
			removePopup();
			finalProcess.exit(true);
		}
		
		private function openPopup():void {
			exportationInProgressWindow = new ExportationWindow();
			exportationInProgressWindow.width = 250;
			exportationInProgressWindow.height = 150;
			exportationInProgressWindow.title = resourceManager.getString('GUI_I18NS', 'import_sound');
			exportationInProgressWindow.setStyle("modalTrasparancy", 0.2);
			exportationInProgressWindow.setStyle("modalTransparencyBlur", 5);
			exportationInProgressWindow.setStyle("modalTransparencyDuration", 1000);
			
			PopUpManager.addPopUp(exportationInProgressWindow,AppFacade.getInstance().app,true);
			PopUpManager.centerPopUp(exportationInProgressWindow);
		}
		
		private function removePopup():void {
			PopUpManager.removePopUp(exportationInProgressWindow);
		}
		
		public function get diskPathsProxy():DiskPathsProxy {
			return facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;;
		}
		
		public function get soundProxy():SoundProxy {
			return facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;;
		}
		
		public function get nativeProcessProxy ():NativeProcessProxy { return facade.retrieveProxy (NativeProcessProxy.NAME) as NativeProcessProxy; }
	}
}