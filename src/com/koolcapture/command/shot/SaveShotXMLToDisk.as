/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
 
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.SoundProxy;
	import com.koolcapture.model.vo.FrameVO;
	import com.koolcapture.model.vo.SoundVO;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SaveShotXMLToDisk extends SimpleCommand {
		override public function execute(notification:INotification):void {
			var xmlToSave:XML= new XML(<shot version="1.0"><timeline><layers></layers></timeline></shot>);
			var layerXML:XML = this.layerToXMl();
			xmlToSave.timeline.layers.appendChild(layerXML);
			var soundXML:XML = this.soundsToXMl();
			xmlToSave.timeline.appendChild(soundXML);
			
			this.saveXMLToDisk(xmlToSave);
		}
		
		private function layerToXMl():XML {
			var shotTimeLineProxy:ShotTimeLineProxy = facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			var shotTimelineXML:XML = new XML(<layer/>);
			
			for each (var frameVO:FrameVO in shotTimeLineProxy.frameVOArrayCollection) {
				if (frameVO.id==ProjectConstant.LIVE_VIEW_FRAMEVO) continue;
				var node:XML=new XML(<image/>);
				node.@id=frameVO.id;
				shotTimelineXML.appendChild(node);
			}
			return shotTimelineXML;
		}
		
		private function soundsToXMl():XML {
			var soundProxy:SoundProxy = facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;
			var soundsXML:XML = new XML(<sounds/>);
			
			for each (var soundVO:SoundVO in soundProxy.soundVOArrayCollection) {
				var node:XML=new XML(<sound/>);
				node.@startpoint=soundVO.start;
				node.@stopposition=soundVO.end;
				node.@id=soundVO.id
				node.@originalname=soundVO.originalName
				soundsXML.appendChild(node);
			}
			return soundsXML;
		}
		
		private function saveXMLToDisk(xml:XML):void {
			var diskPathsProxy:DiskPathsProxy = facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
			var file:File=new File(diskPathsProxy.shotFilePath);
			
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeUTFBytes(xml);
			stream.close();
		}
	}
}