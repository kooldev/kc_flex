/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot
{	
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class GotoLastFrameCmd extends SimpleCommand {
		override public function execute(notification:INotification):void {
			var timeLineProxy:ShotTimeLineProxy = facade.retrieveProxy (ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			
			sendNotification(ShotConstant.GOTO_FRAME, (timeLineProxy.numberFrames - 1));
			/*
			if(cameraProxy.cameraIsActive){
				if (timeLineProxy.numberFrames>=2) {
					sendNotification(ShotConstant.GOTO_FRAME, (timeLineProxy.numberFrames - 2));
				}
				//sendNotification(ShotConstant.SHOW_LIVE_VIDEO);
			}else{
				if (timeLineProxy.numberFrames>=1) {
					
				}
			}
			*/
			sendNotification(ShotConstant.REVERSABLE_FRAMES, false);
		}
		
		public function get cameraProxy ():CameraProxy { return facade.retrieveProxy (CameraProxy.NAME) as CameraProxy; }
	}
}