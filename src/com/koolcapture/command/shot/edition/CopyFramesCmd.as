/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.NativeMenuMediator;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.TimelineEditionProxy;
	
	import flash.desktop.Clipboard;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CopyFramesCmd extends SimpleCommand
	{
		override public function execute(notification:INotification):void
		{
			var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
			var timelineEditionProxy:TimelineEditionProxy= facade.retrieveProxy(TimelineEditionProxy.NAME) as TimelineEditionProxy;
			var nativeMenuMediator:NativeMenuMediator= facade.retrieveMediator(NativeMenuMediator.NAME) as NativeMenuMediator;
			
			if(timeLineMediator.selectedItems.length > 0) { 
				var selectionList:Vector.<int> = timeLineMediator.selectedIndices.sort(sortingIndices);
				var list:Vector.<Object> = new Vector.<Object>();
				var nbObject:int = timeLineMediator.selectedIndices.length;
				for(var i:int =0; i < nbObject; i++) {
					list.push(timeLineMediator.selectedItems[i]);
				}
				
				removeLiveViewFrame(list);
				// Met la liste dans le clipBoard
				if (list.length > 0) {
					Clipboard.generalClipboard.setData("framesList", list, false);
					nativeMenuMediator.setPasteState(true);
					timelineEditionProxy.hasCopiedFrames=true;
					sendNotification(ShotConstant.SET_EDITIBLE_STATE);
				}
			} else { 
				Clipboard.generalClipboard.clear();		
				nativeMenuMediator.setPasteState(false);
			}
		}
		
		private function removeLiveViewFrame(list:Vector.<Object>):void{
			for each (var frame:Object in list){
				if(frame.id == ProjectConstant.LIVE_VIEW_FRAMEVO){
					var index:int = list.indexOf(frame);
					list.splice(index, 1);
					break;
				}
			}
		}
		
		final private function sortingIndices(a:Number, b:Number):int {
		      return (a==b ? 0 : (a < b) ? -1 : 1);
		}
	}
} 