/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition {

	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.model.vo.FrameVO;
	import com.koolcapture.model.ShotTimeLineProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class InsertPhotoBucketFramesCmd extends SimpleCommand {
		
		override public function execute(notification:INotification):void {
			
			sendNotification(ProjectConstant.ADD_HISTORY);
			var framesList:Vector.<Object> =  notification.getBody() as Vector.<Object>;//Clipboard.generalClipboard.getData("framesList", ClipboardTransferMode.ORIGINAL_ONLY) as Vector.<Object>;
			
			if(framesList) {
				//trace("Import "+framesList.length+" frames");
				
				var timeLineProxy:ShotTimeLineProxy = facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
				var frameIndice:int = timeLineProxy.currentIndex + 1;
				
				for each (var frame:Object in framesList){
					frameIndice = timeLineProxy.addFrameAt(frame as FrameVO, frameIndice );
				}
						
				sendNotification(ShotConstant.GOTO_FRAME, frameIndice - 1);
			}
		}
	}
}