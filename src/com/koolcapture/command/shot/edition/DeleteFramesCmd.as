/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition
{
	import com.koolcapture.AppFacade;
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.ProjectProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class DeleteFramesCmd extends SimpleCommand
	{
		private var delayTimer:Timer;
		
		override public function execute(notification:INotification):void {
			sendNotification(ProjectConstant.ADD_HISTORY);
			var appfacade:AppFacade = AppFacade.getInstance();
					
			var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
			var selectItems:Vector.<Object> = timeLineMediator.selectedItems;
			if(containsNoLiveViewFrame(selectItems)){
				Alert.show(appfacade.resourceManager.getString("GUI_I18NS","are_you_you_want_to_delete"), appfacade.resourceManager.getString("GUI_I18NS","delete_files"), Alert.OK    | Alert.CANCEL, null, closing, null);
				var projectProxy:ProjectProxy = facade.retrieveProxy(ProjectProxy.NAME) as ProjectProxy;
				projectProxy.modalWindowIsOpen=true;
			}
		}
		
		private function closing(evt:CloseEvent):void {
			if (evt.detail==Alert.OK) {
				var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
				var timeLineProxy:ShotTimeLineProxy= facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;

				var selectItems:Vector.<Object> = timeLineMediator.selectedItems;
				if (selectItems.length > 0 && containsNoLiveViewFrame(selectItems)) {
					timeLineProxy.removeFrames(timeLineMediator.selectedIndices.concat());
				}
				timeLineProxy.setCurrentFrame(timeLineProxy.currentIndex-1,true);
			}
			
			delayTimer=new Timer(20, 1);
			delayTimer.addEventListener(TimerEvent.TIMER, timerHandler);
			delayTimer.start();
		}
		
		private function containsNoLiveViewFrame(list:Vector.<Object>):Boolean {
			for each (var frame:Object in list){
				if (frame.id == ProjectConstant.LIVE_VIEW_FRAMEVO){
					return false;
				}
			}
			return true;
		}
		
		private function timerHandler(e:TimerEvent):void{
			delayTimer.stop();
			var projectProxy:ProjectProxy = facade.retrieveProxy(ProjectProxy.NAME) as ProjectProxy;
			projectProxy.modalWindowIsOpen=false;
		}

	}
	
}