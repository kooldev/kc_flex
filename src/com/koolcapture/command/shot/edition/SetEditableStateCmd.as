/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.TimelineEditionProxy;
	import com.koolcapture.model.vo.FrameVO;
	import flash.utils.Timer;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SetEditableStateCmd extends SimpleCommand {
		private var force:String;
		private var delayTimer:Timer;
		override public function execute(notification:INotification):void {
			this.force = notification.getBody() as String;
			
			if(this.force==ShotConstant.EDITIBLE_STATE_FORCE_EDITABLE){
				setUpSelection();
				return;
			}
			
			if (isLiveViewSelected() || this.force==ShotConstant.EDITIBLE_STATE_FORCE_NONEDITABLE ) {
				deselectAll();				
			} else {
				setUpSelection();
			}
			
			if (this.force==ShotConstant.EDITIBLE_STATE_FORCE_REVERSABLE) {
				timelineEditionProxy.framesReversable=true;
			}
		}
		
		private function deselectAll():void {
			timelineEditionProxy.framesReversable=false;
			timelineEditionProxy.framesDeletable=false;
			timelineEditionProxy.framesCutAndCopyable=false;
			timelineEditionProxy.framesPastable=Boolean(timelineEditionProxy.hasCopiedFrames);
		}
		
		private function setUpSelection():void {
			if (shotTimelineMediator.selectedItems && shotTimelineMediator.selectedItems.length>1) {
				timelineEditionProxy.framesReversable=true;
			} else {
				timelineEditionProxy.framesReversable=false;
			}
			
			timelineEditionProxy.framesCutAndCopyable=true;
			timelineEditionProxy.framesDeletable=true;
			timelineEditionProxy.framesPastable = timelineEditionProxy.hasCopiedFrames;
		}
		
		private function isLiveViewSelected():Boolean {
			if (shotTimelineMediator.selectedItems.length > 0) { 
				var selectionList:Vector.<int> = shotTimelineMediator.selectedIndices;
				for (var i:int =0; i < selectionList.length; i++) {
					var frame:FrameVO = FrameVO(shotTimelineMediator.selectedItems[i]);
					if (frame.id == ProjectConstant.LIVE_VIEW_FRAMEVO) {
						return true;
					}
				}
				return false;
			}
			return false;
		}
		
		public function get shotTimeLineProxy():ShotTimeLineProxy {
			return facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;;
		}
		
		public function get shotTimelineMediator():ShotTimelineMediator {
			return facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;;
		}
		
		public function get timelineEditionProxy():TimelineEditionProxy {
			return facade.retrieveProxy(TimelineEditionProxy.NAME) as TimelineEditionProxy;;
		}
	}
}