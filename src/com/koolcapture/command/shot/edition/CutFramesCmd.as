/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.NativeMenuMediator;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.TimelineEditionProxy;
	
	import flash.desktop.Clipboard;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CutFramesCmd extends SimpleCommand {
		override public function execute(notification:INotification):void {
			sendNotification(ProjectConstant.ADD_HISTORY);
			var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
			var timeLineProxy:ShotTimeLineProxy = facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			var timelineEditionProxy:TimelineEditionProxy = facade.retrieveProxy(TimelineEditionProxy.NAME) as TimelineEditionProxy;
			var nativeMenuMediator:NativeMenuMediator = facade.retrieveMediator(NativeMenuMediator.NAME) as NativeMenuMediator;
			var firstIndex:int = 0;
			
			if (timeLineMediator.selectedItems.length > 0) {
				// Réordonne la liste  d'indices dans  l'ordre croissant 
				timeLineMediator.selectedIndices.sort(sortingIndices);
				
				// recupere le premier index de la selection
				firstIndex = timeLineMediator.selectedIndices[0];
				
				var list:Vector.<Object> = new Vector.<Object>();
				var nbObject:int = timeLineMediator.selectedIndices.length;
				for (var i:int =0; i < nbObject; i++){
					list.push(timeLineMediator.selectedItems[i]);
				}
				removeLiveViewFrame(list, timeLineMediator.selectedIndices);
				
				if (list.length > 0) {
					Clipboard.generalClipboard.setData("framesList", list, false);
					timeLineProxy.removeFrames(timeLineMediator.selectedIndices.concat());
					nativeMenuMediator.setPasteState(true);
					timelineEditionProxy.hasCopiedFrames=true;
				}
			} else {
				Clipboard.generalClipboard.clear();
				nativeMenuMediator.setPasteState(false);
			}
			
			// Mise a jour de la frame courante
			timeLineProxy.setCurrentFrame(firstIndex,true);
			
			if ((firstIndex == (timeLineProxy.numberFrames-1) && cameraProxy.cameraIsActive)){
				sendNotification(ShotConstant.SET_EDITIBLE_STATE, ShotConstant.EDITIBLE_STATE_FORCE_NONEDITABLE);
			}
		}
		
		private function removeLiveViewFrame(list:Vector.<Object>, selectedIndices:Vector.<int>):void{
			for each (var frame:Object in list){
				if(frame.id == ProjectConstant.LIVE_VIEW_FRAMEVO){
					var index:int = list.indexOf(frame);
					list.splice(index, 1);
					selectedIndices.splice(index, 1);
					break;
				}
			}
		}
		
		final private function sortingIndices(a:Number, b:Number):int {
		      return (a==b ? 0 : (a < b) ? -1 : 1);
		}
		
		public function get cameraProxy ():CameraProxy { return facade.retrieveProxy (CameraProxy.NAME) as CameraProxy; }
	}
}