/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.shot.edition
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.vo.FrameVO;
	
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardTransferMode;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class PasteFramesBeforeCmd extends SimpleCommand
	{
		
		override public function execute(notification:INotification):void {
			sendNotification(ProjectConstant.ADD_HISTORY);
			var framesList:Vector.<Object> = Clipboard.generalClipboard.getData("framesList", ClipboardTransferMode.ORIGINAL_ONLY) as Vector.<Object>;
			
			if(framesList) {
				var selectList:Vector.<int> = new Vector.<int>();
								
				var timeLineProxy:ShotTimeLineProxy = facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
				var frameIndice:int = timeLineProxy.currentIndex;
				for (var i:int=framesList.length-1; i>=0; i--) {
					var frame:Object = framesList[i];
					frameIndice = timeLineProxy.addFrameAt(frame as FrameVO, frameIndice);
					selectList.push(frameIndice);
				}				
				
				timeLineProxy.setCurrentFrame(frameIndice,true);
				if(selectList.length > 1){
					var timeLineMediator:ShotTimelineMediator = facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;
					timeLineMediator.selectedIndices = selectList;
				}
				sendNotification(ShotConstant.SET_EDITIBLE_STATE);
			}
		}
	}
}