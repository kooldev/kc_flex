package com.koolcapture.command.project {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.model.CameraProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ActiveCameraCmd extends SimpleCommand {
		override public function execute(notification:INotification):void{
			var cameraActive:Boolean=notification.getBody() as Boolean;
			cameraProxy.cameraIsActive = cameraActive;
			
			var shotTimeLineProxy:ShotTimeLineProxy = facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;
			shotTimeLineProxy.addCameraFrameVOIfNeeded();
			sendNotification(ProjectConstant.CAMERA_ACTIVATED, cameraActive);
		}
		
		public function get cameraProxy():CameraProxy {
			return facade.retrieveProxy(CameraProxy.NAME) as CameraProxy;
		}
	}
}