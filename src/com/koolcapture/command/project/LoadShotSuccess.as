/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.project {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.mediator.MonitorMediator;
	import com.koolcapture.mediator.NativeMenuMediator;
	import com.koolcapture.mediator.TakeViewMediator;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.PreferencesProxy;
	import com.koolcapture.model.TimelineEditionProxy;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class LoadShotSuccess extends SimpleCommand {
		override public function execute(notification:INotification):void {
			var diskPathsProxy:DiskPathsProxy= facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;			
			var takeViewMediator:TakeViewMediator= facade.retrieveMediator(TakeViewMediator.NAME) as TakeViewMediator;
			takeViewMediator.setTakeTitle(diskPathsProxy.projectName +" | "+ diskPathsProxy.shotName );
			
			var preferencesProxy:PreferencesProxy= facade.retrieveProxy(PreferencesProxy.NAME) as PreferencesProxy;
			preferencesProxy.recentFilm=diskPathsProxy.projectName;
			preferencesProxy.recentShot=diskPathsProxy.shotName;
			sendNotification(ProjectConstant.PREFERENCESCHANGED);
			
			var nativeMenuMediator:NativeMenuMediator= facade.retrieveMediator(NativeMenuMediator.NAME) as NativeMenuMediator;
			nativeMenuMediator.takeOpenedState();
			var monitorMediator:MonitorMediator= facade.retrieveMediator(MonitorMediator.NAME) as MonitorMediator;
			timelineEditionProxy.reset();
		}
		
		public function get timelineEditionProxy():TimelineEditionProxy {
			return facade.retrieveProxy(TimelineEditionProxy.NAME) as TimelineEditionProxy;
		}
	}
}