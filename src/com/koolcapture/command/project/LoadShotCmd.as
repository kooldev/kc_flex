/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.project
{
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.mediator.ShotTimelineMediator;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.ShotTimeLineProxy;
	import com.koolcapture.model.SoundProxy;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class LoadShotCmd extends SimpleCommand {
		override public function execute(notification:INotification):void {			
			var diskPathsProxy:DiskPathsProxy= facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
			var timelinefile:File=new File(diskPathsProxy.shotFilePath);
			
			if (timelinefile.exists) {
				var timelineStream:FileStream= new FileStream();
				timelineStream.open(timelinefile, FileMode.READ);
				var xml:XML = new XML();
				xml = XML(timelineStream.readUTFBytes(timelineStream.bytesAvailable));
				timelineStream.close();
				shotTimeLineProxy.currentIndex = 0;
				shotTimeLineProxy.initData(xml.timeline.layers);
				soundProxy.initData(xml.timeline.sounds);
				shotTimelineMediator.frameArrayCollection=shotTimeLineProxy.frameVOArrayCollection;
				
				sendNotification(ShotConstant.GOTO_LAST_FRAME);
				sendNotification(ProjectConstant.LOAD_SHOT_SUCCESS);
			} else {
				throw new Error("Shot file does not exist");
			}
		}
		
		public function get soundProxy():SoundProxy {
			return facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;;
		}
		
		public function get shotTimeLineProxy():ShotTimeLineProxy {
			return facade.retrieveProxy(ShotTimeLineProxy.NAME) as ShotTimeLineProxy;;
		}
		
		public function get shotTimelineMediator():ShotTimelineMediator {
			return facade.retrieveMediator(ShotTimelineMediator.NAME) as ShotTimelineMediator;;
		}
	}
}
