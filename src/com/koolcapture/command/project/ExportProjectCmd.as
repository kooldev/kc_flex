package com.koolcapture.command.project {
	import com.koolcapture.AppFacade;
	import com.koolcapture.model.DiskPathsProxy;
	import com.koolcapture.model.PreferencesProxy;
	import com.koolcapture.mxml.window.ExportationWindow;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	
	public class ExportProjectCmd extends SimpleCommand {
		private var resourceManager:IResourceManager;
		private var exportationInProgressWindow:ExportationWindow;
		private var filePaths:Object = new Object();
		private var shotNamesArray:Array = new Array();
		private var totalFiles:Number=0;
		private var targetDirectory:File;
		private var copyToFolder:File;
		private var shotIndex:Number = 0;
		private var actualImageIndex:Number = 0;
		private var timer:Timer;
		private var progress:int = 0;
		
		override public function execute(notification:INotification):void {
			this.resourceManager= AppFacade.getInstance().resourceManager;
			var dialog:File= new File();
			dialog.addEventListener(Event.SELECT, directorySelected);
			dialog.addEventListener(Event.CANCEL, canceled);
			
			dialog.browseForDirectory(resourceManager.getString('GUI_I18NS'
				,'choose_a_folder'));
		}
		
		private function removePopup():void {
			PopUpManager.removePopUp(exportationInProgressWindow);
		}
		
		private function canceled(event:Event):void{
			removePopup();
		}
		
		private function openPopup():void {
			exportationInProgressWindow = new ExportationWindow();
			exportationInProgressWindow.width = 250;
			exportationInProgressWindow.height = 150;
			
			exportationInProgressWindow.title = resourceManager.getString('GUI_I18NS', 'export_in_progress');
			exportationInProgressWindow.setStyle("modalTrasparancy",0.2);
			exportationInProgressWindow.setStyle("modalTransparencyBlur", 5);
			exportationInProgressWindow.setStyle("modalTransparencyDuration",50);
			
			PopUpManager.addPopUp(exportationInProgressWindow,AppFacade.getInstance().app,true);
			PopUpManager.centerPopUp(exportationInProgressWindow);
		}
		
		private function directorySelected(event:Event):void  {
			this.targetDirectory = event.target as File;
			var shotsFolderPath:File = new File(diskPathsProxy.shotsFolderPath)
			if (shotsFolderPath.isDirectory) {
				var files:Array= shotsFolderPath.getDirectoryListing()
				for(var i:String in files){
					var file:File = files[i];
					if(file.isDirectory){
						createData(file.name);
					}
				}
				checkToDestination();
			}
		}
		
		public function createData(shot:String):void {
			var shotPathFolder:File=new File(diskPathsProxy.shotsFolderPath).resolvePath(shot);
			var timelineFile:File=shotPathFolder.resolvePath(diskPathsProxy.SHOT_FILE_NAME);
			var timelineStream:FileStream= new FileStream();
			timelineStream.open(timelineFile, FileMode.READ);
			var xml:XML = new XML();
			xml = XML(timelineStream.readUTFBytes(timelineStream.bytesAvailable));
			timelineStream.close();
			var imageFolder:File = shotPathFolder.resolvePath(diskPathsProxy.PNG_IMAGE_FOLDERNAME);
			var shotFiles:Array = [];
			for each (var frame:XML in xml.timeline.layers.layer.image) {
				var imageName:String = diskPathsProxy.IMAGE_PREFIX + frame.@id + ".png"
				var imageFile:File = imageFolder.resolvePath(imageName)
				
				shotFiles.push(imageFile);
				totalFiles++;
			}
			
			filePaths[shot]=shotFiles;
			shotNamesArray.push(shot);
		}
		
		public function checkToDestination():void{
			var projectDirectory:File = new File(diskPathsProxy.projectFolderPath);
			var targetDirectory:File = new File(targetDirectory.nativePath);
			var appfacade:AppFacade = AppFacade.getInstance();
			if (String(targetDirectory.nativePath).indexOf(preferencesProxy.workspaceDirectory) !== -1) {
				Alert.show(appfacade.resourceManager.getString("GUI_I18NS","cant_export_to_workspace"), appfacade.resourceManager.getString("GUI_I18NS","choose_another_project_folder"), Alert.YES, null, null);
				return;
			}
			
			this.copyToFolder;
			if (diskPathsProxy.projectName === this.targetDirectory.name) {
				copyToFolder=this.targetDirectory;
				copyToImagesFolder(copyToFolder);
			} else {
				copyToFolder=targetDirectory.resolvePath(diskPathsProxy.projectName);
				if (copyToFolder.exists){
					Alert.show(appfacade.resourceManager.getString("GUI_I18NS","do_you_wish_to_overwrite_folder"), appfacade.resourceManager.getString("GUI_I18NS","destination_folder_exists"), Alert.YES|Alert.NO, null, alertCloseHandler);
				} else {
					copyToFolder.createDirectory();
					copyToImagesFolder(copyToFolder);
				}
			}
		}
		
		private function alertCloseHandler(e:CloseEvent):void {
			if(Number(e.detail) === Number(Alert.YES)){
				copyToImagesFolder(copyToFolder);
			}
		}

		private function copyToImagesFolder(copyToFolder:File):void {
			if (copyToFolder.exists){
				copyToFolder.deleteDirectory(true);
				copyToFolder.createDirectory();
			}
			
			openPopup();
			
			var shotIndex:Number = 0;
			var actualImageIndex:Number = 0;
			
			timer = new Timer(1, 0);
			timer.addEventListener(TimerEvent.TIMER, pauseATick);
			timer.start();
		}
	
		private function pauseATick (e:TimerEvent):void {
			var shotName:String = shotNamesArray[shotIndex];
			var shotFolder:File= copyToFolder.resolvePath(shotName);
			
			var image:File = filePaths[shotName][actualImageIndex];
			if (image) {
				var imageCopy:File = shotFolder.resolvePath(imageNameFromNumber(Number(actualImageIndex)+1));
				var percentage:int = Math.floor((progress-1)/totalFiles*100);
				//trace(imageCopy.nativePath);
				
				exportationInProgressWindow.exportProgressBar.setProgress(percentage, 100);
				exportationInProgressWindow.exportProgressBar.label=resourceManager.getString('GUI_I18NS', 'exported')+" "+ String(percentage)+"%";
				if (image.exists) { 
					image.copyToAsync(imageCopy); 
				}
			}
			
			progress++;
			actualImageIndex++;
			if ( actualImageIndex >= filePaths[shotName].length ) {
				actualImageIndex = 0;
				shotIndex++;
				if (shotIndex >= shotNamesArray.length){
					timer.stop();
					removePopup();
				}
			}
		}
		
		private function imageNameFromNumber(digit:Number):String {
			var numberOfZeros:Number = 7- String(digit).length;
			var zeroString:String="";
			for (var i:Number=0; i<numberOfZeros; i++) {
				zeroString+="0";	
			}
			var imageName:String = "image" + zeroString + digit + ".png";
			return imageName
		}
		
		private function get diskPathsProxy ():DiskPathsProxy {
 			return facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
		}	
		
		private function get preferencesProxy ():PreferencesProxy {
			return facade.retrieveProxy(PreferencesProxy.NAME) as PreferencesProxy;
		}	
	}
}