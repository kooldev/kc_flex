/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.command.project {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.model.DiskPathsProxy;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CreateShotCmd extends SimpleCommand {
		override public function execute(notification:INotification):void {
			var  diskPathProxy:DiskPathsProxy= facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;
			
			var file:File = new File(diskPathProxy.shotFilePath);
			
			if(!file.exists){
				
				var shotSourceFolder:File= new File(diskPathProxy.shotFolderPath);
				if(!shotSourceFolder.exists){
					shotSourceFolder.createDirectory();
				}
				
				var shotThumbFolderPath:File= new File(diskPathProxy.shotThumbFolderPath);
				if(!shotThumbFolderPath.exists){
					shotThumbFolderPath.createDirectory();
				}
				
				var shotPrevFolderPath:File= new File(diskPathProxy.shotPrevFolderPath);
				if(!shotPrevFolderPath.exists){
					shotPrevFolderPath.createDirectory();
				}
				
				var shotPNGFolderPath:File= new File(diskPathProxy.shotPNGFolderPath);
				if(!shotPNGFolderPath.exists){
					shotPNGFolderPath.createDirectory();
				}
				var xml:XML = <shot version="1.0">
								<timeline>
									<layers><layer/></layers>
									<sounds/>
								</timeline>
							   </shot>;
				
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.WRITE);
				stream.writeUTFBytes(xml);
				stream.close();
			}
			
			sendNotification(ProjectConstant.LOAD_SHOT);
		}
	}
}