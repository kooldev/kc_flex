/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.constant {
	public class ShotConstant {
		public static const SET_EDITIBLE_STATE:String					= "ShotConstant.SET_EDITIBLE_STATE";
		public static const EDITIBLE_STATE_FORCE_NONEDITABLE:String		= "ShotConstant.EDITIBLE_STATE_FORCE_NONEDITABLE";
		public static const EDITIBLE_STATE_FORCE_EDITABLE:String		= "ShotConstant.EDITIBLE_STATE_FORCE_EDITABLE";
		public static const EDITIBLE_STATE_FORCE_REVERSABLE:String		= "ShotConstant.EDITIBLE_STATE_FORCE_REVERSABLE";
		public static const COPY_FRAMES:String							= "ShotConstant.COPY_FRAMES";
		public static const FRAMES_COPIED:String						= "ShotConstant.FRAMES_COPIED";
		public static const PASTE_FRAMES_BEFORE:String					= "ShotConstant.PASTE_FRAMES_BEFORE";
		public static const PASTE_FRAMES_AFTER:String					= "ShotConstant.PASTE_FRAMES_AFTER";
		public static const CUT_FRAMES:String							= "ShotConstant.CUT_FRAMES";
		public static const CUT_AND_COPYABLE:String						= "ShotConstant.CUT_AND_COPYABLE";
		public static const DELETE_FRAMES:String						= "ShotConstant.DELETE_FRAMES";
		public static const REVERSE_FRAMES:String						= "ShotConstant.REVERSE_FRAMES";
		public static const REVERSABLE_FRAMES:String					= "ShotConstant.REVERSABLE_FRAMES";
		public static const DELETABLE_FRAMES:String						= "ShotConstant.DELETABLE_FRAMES";
		public static const PASTABLE_FRAMES:String						= "ShotConstant.PASTABLE_FRAMES";
		public static const PASTE_INVERTED_FRAMES:String				= "ShotConstant.PASTE_INVERTED_FRAMES";
		public static const IMPORT_IMAGE:String							= "ShotConstant.IMPORT_IMAGE";
		public static const IMPORT_SOUND:String							= "ShotConstant.IMPORT_SOUND";
		public static const DELETE_SOUND:String							= "ShotConstant.DELETE_SOUND";
		public static const LOAD_FRAMES:String							= "ShotConstant.LOAD_FRAMES";
		public static const LOAD_FRAMES_SUCCESS:String					= "ShotConstant.LOAD_FRAMES_SUCCESS";
		public static const CAPTURE_FRAMES:String						= "ShotConstant.CAPTURE_FRAMES";
		public static const CAPTURE_FRAMES_SUCCESS:String				= "ShotConstant.CAPTURE_FRAMES_SUCCESS";
		public static const ONIONSKIN_ALPHA_VALUE_CHANGE:String			= "ShotConstant.ONIONSKIN_ALPHA_VALUE_CHANGE";
		
		/** TRANSPORT */
		public static const CURRENT_FRAME_CHANGED:String				= "ShotConstant.CURRENT_FRAME_CHANGED";
		public static const GOTO_FIRST_FRAME:String						= "ShotConstant.GOTO_FIRST_FRAME";
		public static const GOTO_LAST_FRAME:String						= "ShotConstant.GOTO_LAST_FRAME";
		public static const GOTO_SECOND_LAST_FRAME:String				= "ShotConstant.GOTO_SECOND_LAST_FRAME";
		public static const TRANSPORT_PREPARE_TO_PLAY:String			= "ShotConstant.TRANSPORT_PREPARE_TO_PLAY";
		public static const TRANSPORT_PLAY:String						= "ShotConstant.TRANSPORT_PLAY";
		public static const TRANSPORT_STOP:String						= "ShotConstant.TRANSPORT_STOP";
		public static const TRANSPORT_SWITCH_PLAY:String				= "ShotConstant.TRANSPORT_SWITCH_PLAY";
		public static const TRANSPORT_SWITCH_LOOP:String				= "ShotConstant.TRANSPORT_SWITCH_LOOP";
		public static const TRANSPORT_SET_FPS:String					= "ShotConstant.TRANSPORT_SET_FPS";
		public static const TRANSPORT_FPS_CHANGED:String				= "CaptureContant.transport_fps_changed"
		public static const TRANSPORT_SET_PLAYBACK_QUALITY:String		= "ShotConstant.TRANSPORT_SET_PLAYBACK_QUALITY";
		public static const TRANSPORT_PLAYBACK_QUALITY_CHANGED:String 	= "ShotConstant.PLAYBACK_QUALITY_CHANGED"
		public static const TRANSPORT_SHORT_PLAYBACK_CHANGED:String 	= "ShotConstant.TRANSPORT_SHORT_PLAYBACK_CHANGED"
		public static const GOTO_NEXT_FRAME:String						= "ShotConstant.GOTO_NEXT_FRAME";
		public static const GOTO_PREV_FRAME:String						= "ShotConstant.GOTO_PREV_FRAME";	
		public static const GOTO_FRAME:String							= "ShotConstant.GOTO_FRAME";
		public static const SELECT_ALL_FRAMES:String					= "ShotConstant.SELECT_ALL_FRAMES";
		public static const DUPLICATE_FRAMES:String						= "ShotConstant.DUPLICATE_FRAMES";
		public static const TOGGLE_LIVE_VIDEO:String					= "ShotConstant.TOGGLE_LIVE_VIDEO";
		public static const TOGGLE_GRID:String							= "ShotConstant.TOGGLE_GRID";
		public static const GRID_TOGGLED:String							= "ShotConstant.GRID_TOGGLED";
		public static const TOGGLE_VERTICAL_FLIP:String					= "ShotConstant.TOGGLE_VERTICAL_FLIP";
		public static const TOGGLE_HORIZONTAL_FLIP:String				= "ShotConstant.TOGGLE_HORIZONTAL_FLIP";
		public static const TOGGLE_VERTICAL_FLIPPED:String				= "ShotConstant.TOGGLE_VERTICAL_FLIPPED";
		public static const TOGGLE_HORIZONTAL_FLIPPED:String			= "ShotConstant.TOGGLE_HORIZONTAL_FLIPPED";
		public static const LIVE_VIDEO_SHOWN:String						= "ShotConstant.LIVE_VIDEO_SHOWN";
		public static const LIVE_VIDEO_HIDDEN:String					= "ShotConstant.LIVE_VIDEO_HIDDEN";
		public static const EXPORT_TAKE_IMAGES:String 					= "ShotConstant.EXPORT_TAKE_IMAGES";
		public static const EXPORT_FILM:String 							= "ShotConstant.EXPORT_FILM";
		public static const TAKETIMELINEMEDIATOR_FRAME_CHANGED:String	= "ShotConstant.TAKETIMELINE_MEDIATOR_FRAME_CHANGED";
		public static const CAPTURE_ACTIVATE:String 					= "ShotConstant.CAPTURE_ACTIVATE";
		public static const CAPTURE_DISACTIVATE:String 					= "ShotConstant.CAPTURE_DISACTIVATE";
		public static const INSERT_PHOTO_BUCKET_FRAMES:String 			= "ShotConstant.INSERT_PHOTO_BUCKET_FRAMES";
		public static const SELECT_NEXT_FRAME:String 					= "ShotConstant.SELECT_NEXT_FRAME";
		public static const SELECT_PREVIOUS_FRAME:String 				= "ShotConstant.SELECT_PREVIOUS_FRAME";
		public static const TOGGLE_LIVE_VIEW_STATE:String 				= "ShotConstant.TOGGLE_LIVE_VIEW_STATE";
		public static const TOGGLE_SHORTPLAY:String						= "ShotConstant.TOGGLE_SHORTPLAY";
		public static const TIMELINE_ZOOM_IN:String 					= "ShotConstant.TIMELINE_ZOOM_IN";
		public static const TIMELINE_ZOOM_OUT:String 					= "ShotConstant.TIMELINE_ZOOM_OUT";
		public static const TIMELINE_ZOOMED_IN:String 					= "ShotConstant.TIMELINE_ZOOMED_IN";
		public static const TIMELINE_ZOOMED_OUT:String 					= "ShotConstant.TIMELINE_ZOOMED_OUT";
		public static const CAPTURE_NUMBER_CHANGED:String 				= "ShotConstant.CAPTURE_NUMBER_CHANGED";
		public static const SAVE_DATA_TO_DISK:String 					= "ShotConstant.SAVE_DATA_TO_DISK";
		public static const HAS_SOUND:String 							= "ShotConstant.HAS_SOUND";
	}	
}