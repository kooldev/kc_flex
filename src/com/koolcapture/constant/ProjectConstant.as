/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.constant {
	public class ProjectConstant {
		public static const LIVE_VIEW_FRAMEVO:String								= "ProjectConstant.LIVE_VIEW_FRAMEVO";
		public static const DOWNLOAD_NEW_VERSION:String								= "ProjectConstant.DOWNLOAD_NEW_VERSION";
		public static const OPEN_ABOUT_WINDOW:String								= "ProjectConstant.OPEN_ABOUT_WINDOW";
		public static const OPEN_PROJECT_MANAGER_WINDOW:String						= "ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW";
		public static const OPEN_PROJECT_MANAGER_WINDOW_WITHOUT_CLOSE_BUTTON:String	= "ProjectConstant.OPEN_PROJECT_MANAGER_WINDOW_WITHOUT_CLOSE_BUTTON";
		public static const OPEN_TIMELAPSE_WINDOW:String							= "ProjectConstant.OPEN_TIMELAPSE_WINDOW";
		public static const OPEN_PHOTO_BUCKET_WINDOW:String							= "ProjectConstant.OPEN_PHOTO_BUCKET_WINDOW";
		public static const SETUPMENU:String										= "ProjectConstant.SETUPMENU";
		public static const OPEN_PREFERENCEWINDOW:String							= "ProjectConstant.OPEN_PREFERENCEWINDOW";
		public static const OPEN_EXPORT_WINDOW:String								= "ProjectConstant.OPEN_EXPORT_WINDOW";
		public static const PREFERENCES_LOAD:String 								= "ProjectConstant.PREFERENCES_LOAD";
		public static const PREFERENCES_LOADED:String 								= "ProjectConstant.PREFERENCES_LOADED";
		public static const PREFERENCESCHANGED:String								= "ProjectConstant.PREFERENCESCHANGED";
		public static const PREFERENCE_WORKSPACE_CHANGED:String     				= "ProjectConstant.PREFERENCE_WORKSPACE_CHANGED";
		public static const PREFERENCE_LANGUAGE_CHANGED:String    					= "ProjectConstant.PREFERENCE_LANGUAGE_CHANGED";
		public static const LANGUAGE_SET:String    									= "ProjectConstant.LANGUAGE_SET";
		
		/* Project Creation */
		public static const CREATE_PROJECT:String    								= "ProjectConstant.CREATE_PROJECT";
		public static const CREATE_PROJECT_SUCCESS:String   						= "ProjectConstant.CREATE_PROJECT_SUCCESS";
		public static const LOAD_PROJECT:String    									= "ProjectConstant.LOAD_PROJECT";
		public static const CREATE_AND_LOAD_SHOT:String     						= "ProjectConstant.CREATE_AND_LOAD_SHOT";
		public static const CREATE_SHOT_SUCCESS:String      						= "ProjectConstant.CREATE_SHOT_SUCCESS";
		public static const LOAD_SHOT:String    									= "ProjectConstant.LOAD_SHOT";
		public static const LOAD_SHOT_SUCCESS:String    							= "ProjectConstant.LOAD_SHOT_SUCCESS";
		public static const SAVE_SHOT:String     									= "ProjectConstant.SAVE_TAKE";
		public static const ADD_HISTORY:String     									= "ProjectConstant.ADD_HISTORY";
		public static const UNDO_HISTORY:String     								= "ProjectConstant.UNDO_HISTORY";
		public static const REDO_HISTORY:String     								= "ProjectConstant.REDO_HISTORY";
		
		public static const FULLSCREEN:String 										= "ProjectConstant.FULLSCREEN";
		public static const FULLSCREEN_LEFT:String 									= "ProjectConstant.FULLSCREEN_LEFT";
		public static const FULLSCREEN_ENTERED:String 								= "ProjectConstant.FULLSCREEN_ENTERED";
		public static const TOGGLE_FULLSCREEN:String								= "ProjectConstant.TOGGLE_FULLSCREEN";
		public static const CHECK_FOR_UPDATES:String								= "ProjectConstant.CHECK_FOR_UPDATES";
		public static const TOGGLE_AUTOMATIC_UPDATES:String							= "ProjectConstant.TOGGLE_AUTOMATIC_UPDATES";
		public static const CHANGE_CAMERA:String									= "ProjectConstant.CHANGE_CAMERA";
		public static const ACTIVATE_CAMERA:String									= "ProjectConstant.ACTIVATE_CAMERA";
		public static const CAMERA_ACTIVATED:String									= "ProjectConstant.CAMERA_ACTIVATED";
		public static const EXPORT_PROJECT:String									= "ProjectConstant.EXPORT_PROJECT";
	}
}