/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.model {
	import com.koolcapture.model.PreferencesProxy;
	import com.koolcapture.model.vo.FrameFileVO;
	import com.koolcapture.model.vo.FrameVO;
	
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	public class DiskPathsProxy extends Proxy implements IProxy {
		public static const NAME:String="DiskPathsProxy";
		private var _projectName:String;
		private var _shotName:String=null;
		private var _takeName:String=null;
		public const PREFERENCE_LIST_PATH:String = "settings/preference.xml";
		public const SHOTS_FOLDERNAME:String = "shots";
		private const PROJECT_FILE_NAME:String	= "project.kcp";
		public const SHOT_FILE_NAME:String	= "shot.xml";
		private const PREVIEW_IMAGE_FOLDERNAME:String = "prev";
		private const THUMB_IMAGE_FOLDERNAME:String = "thumb";
		public const PNG_IMAGE_FOLDERNAME:String = "png";
		public const IMAGE_PREFIX:String = "view";
		private const SOUND_FOLDERNAME:String = "sound";
		
		public function DiskPathsProxy(proxyName:String=NAME, data:Object=null) {
			super(proxyName, data);
		}
		
		public function setCurrentTake(aProjectName:String, aShotName:String):void{
			this._projectName=aProjectName;
			this._shotName=aShotName;
		}
		
		/* PATHS */
		public function get workSpaceFolder():String {
			return preferenceProxy.workspaceDirectory+"/";
		}
		
		public function set projectName(aProjectName:String):void {
			_projectName=aProjectName;
			if (!new File(projectFolderPath).exists) { trace("WARNING : Project Does Not Exist !! " + projectFolderPath); }
		}
		
		public function get projectName():String{
			return _projectName;
		}
		
		public function set shotName(aShotName:String):void {
			_shotName=aShotName;
		}
		
		public function get shotName():String{
			return _shotName;
		}
		
		
		public function get projectFolderPath():String {
			return workSpaceFolder+_projectName+"/";
		}
		
		public function get projectFilePath():String {
			return projectFolderPath+PROJECT_FILE_NAME;
		}		
		
		public function get shotsFolderPath():String {
			return projectFolderPath+SHOTS_FOLDERNAME+"/"
		}
		
		public function get shotFolderPath ():String{
			return shotsFolderPath+_shotName+"/"
		}
		
		public function get shotFilePath ():String{
			return shotFolderPath+SHOT_FILE_NAME;
		}
		
		public function get soundFolderPath ():String {
			return shotFolderPath + "/" + SOUND_FOLDERNAME;
		}
		
		public function get shotPNGFolderPath ():String{
			return shotFolderPath+"/"+PNG_IMAGE_FOLDERNAME+"/";
		}
		
		public function get shotThumbFolderPath ():String{
			return shotFolderPath+"/"+THUMB_IMAGE_FOLDERNAME+"/";
		}
		public function get shotPrevFolderPath ():String{
			return shotFolderPath+"/"+PREVIEW_IMAGE_FOLDERNAME+"/";
		}
		
		public function  getImagename (id:String):String { 
			return "view"+id+".png";  
		}
		
		public function getThumbimage(id:String):String {
			return "thumb"+id+".png"; 
		}
		
		public function getPreviewimage(id:String):String {
			return "prev"+id+".png"; 
		}
		
		public function getFrameFileVO(id:String):FrameFileVO{
			var imageUrl:String = shotPNGFolderPath+getImagename(id);
			var thumbUrl:String = shotThumbFolderPath+getThumbimage(id);
			var previewUrl:String = shotPrevFolderPath+getPreviewimage(id);
			
			return new FrameFileVO(imageUrl, thumbUrl, previewUrl);
		}
		
		public function createFrameId():String {
			// TODO : totaly unsecure ! si on efface un fichier c la merde
			// Determine le nombre de fichier presents
			var src:File = File.userDirectory.resolvePath(shotPNGFolderPath);
			var fileCount:int = 0;
			for each (var file:File in src.getDirectoryListing()){
				if(file.extension == "png")
					fileCount++;
			}
			
			var suffix:String		= String(fileCount+1);
			var lengthSuffix:int	= 7 - suffix.length;
			var prefix:String		= "";
			for(var i:int = 0; i < lengthSuffix; i++){
				prefix += "0";
			}
			return prefix + suffix;
		}
		
		public function createPhotoBucketFrames():ArrayCollection {
			var source:File=File.userDirectory.resolvePath(shotPNGFolderPath);
			var photoBucketFrameIdArray:Array=new Array();
			if(source.exists){
				
				var fileContentsArray:Array=source.getDirectoryListing();
				
				for each (var file:File in fileContentsArray){
					if(!file.isHidden){
						var idRegExp:RegExp = /view(.*?).png/g;
						var matches:Object = idRegExp.exec(String(file.name));
						photoBucketFrameIdArray.push(matches[1]);
					}
				}
			}else{
				throw new Error("Dossier SRC n'exist pas");
			}
			
			var photoBucketFramesArray:Array = new Array();		
			for each (var id:String in photoBucketFrameIdArray) {
				photoBucketFramesArray.push(new FrameVO(id, getFrameFileVO(id)));
			}
			
			var photoBucketFramesArrayCollection:ArrayCollection= new ArrayCollection(photoBucketFramesArray);
			return photoBucketFramesArrayCollection;
		}
		
		public function get preferenceProxy():PreferencesProxy {
			return facade.retrieveProxy(PreferencesProxy.NAME) as PreferencesProxy;;
		}
	}
}