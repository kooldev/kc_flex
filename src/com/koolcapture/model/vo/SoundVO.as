package com.koolcapture.model.vo {
	import com.koolcapture.tools.Primitive;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	public class SoundVO extends EventDispatcher {
		public static const SOUND_LOADED:String="soundloaded";
		private var _id:String;
		private var _originalName:String;
		private var _importExtension:String;
		private var _start:int=0;
		private var _end:int=-1;
		public var sound:Sound;
		
		public function SoundVO() {}
		
		public function init(xml:XMLList):void{
			this._id=xml.attribute("id");
			this._start=xml.attribute("start");
			this._end=xml.attribute("end");
			this._originalName=xml.attribute("originalname");
		}
				
		public function loadSound(path:String):void {
			if (this.originalName) {
				this.sound = new Sound();
				var soundFile:File = new File(path);
				var path:String=soundFile.nativePath;
				if ( Primitive.os() == Primitive.MAC ) { path="file:/"+path; }
				if ( soundFile.exists ) { this.sound.load( new URLRequest(path) ); }
			}
		}
		
		public function get id():String{
			return this._id;
		}
		
		public function set id(id:String):void {
			this._id=id;
		}
		
		public function get start():int{
			return this._start;
		}
		
		public function set start(startPosition:int):void {
			this._start=start;
		}
		
		public function get end():int{
			return this._end;
		}
		
		public function set end(stopPosition:int):void {
			this._end=end;
		}
		
		public function get originalName():String{
			return this._originalName;
		}
		
		public function set originalName(originalName:String):void {
			this._originalName=originalName;
		}
	}
}