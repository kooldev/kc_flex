/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.model {
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.model.vo.SoundVO;
	
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class SoundProxy extends Proxy implements IProxy {
		public static const NAME:String="SoundProxy";
		private var _sound:Sound;
		private var _soundChannel:SoundChannel;
		private var _soundVO:SoundVO;
		private var _soundVOArrayCollection:ArrayCollection;
		
		public function SoundProxy ( proxyName:String=NAME, data:Object=null ) {
			super (proxyName, data);
		}
		
		public function play():void {
			if (this._soundVO) {
				_soundChannel=this._soundVO.sound.play((takeProxy.currentIndex/takeProxy.fps)*1000);
			}
		}
		
		public function stop():void {
			if (this._soundVO && _soundChannel) {
				_soundChannel.stop();
			}
		}
		
		public function addSound(name:String):void {
			if (this.soundVO){ removeSound(this.soundVO); };
			this.soundVO=new SoundVO();
			this.soundVO.id=String(soundVOArrayCollection.length+1);
			this.soundVO.originalName=name;
			this.loadSound(this.soundVO);
			soundVOArrayCollection.addItem(this.soundVO);
			sendNotification(ShotConstant.HAS_SOUND, true);
			sendNotification(ShotConstant.SAVE_DATA_TO_DISK);
		}
		
		public function removeSound(soundVO:SoundVO):void {
			this.soundVO=null;
			soundVOArrayCollection.removeItemAt(soundVOArrayCollection.getItemIndex(soundVO));
			sendNotification(ShotConstant.SAVE_DATA_TO_DISK);
		}
		
		public function set soundVOArrayCollection(soundVOArrayCollection:ArrayCollection):void {
			this._soundVOArrayCollection=soundVOArrayCollection;
		}
		
		public function get soundVOArrayCollection():ArrayCollection {
			return this._soundVOArrayCollection;
		}
		
		public function set soundVO(soundVO:SoundVO):void {
			this._soundVO=soundVO;
		}
		
		public function get soundVO():SoundVO {
			return this._soundVO;	
		}
		
		public function loadSound(soundVO:SoundVO):void {
			var soundFile:File= new File( diskPathsProxy.soundFolderPath+"/"+soundVO.originalName);
			if (soundFile.exists) {
				soundVO.loadSound(soundFile.nativePath);
			}
		}
		
		public function initData(xml:XMLList):void {
			this._soundVO =null;
			this.soundVOArrayCollection = new ArrayCollection ();
			
			if (xml.sound.length()==0) {
				sendNotification(ShotConstant.HAS_SOUND, false);
				return;
			}
			
			for each (var sound:XML in xml.sound) {
				this.soundVO = new SoundVO();
				this.soundVO.id=sound.attribute("id");
				this.soundVO.originalName=sound.attribute("originalname");
				this.soundVO.start=sound.attribute("start");
				this.soundVO.end=sound.attribute("end");
				
				soundVOArrayCollection.addItem(this.soundVO);
			}
			
			sendNotification(ShotConstant.HAS_SOUND, true);
			
			this.loadSound(SoundVO(soundVOArrayCollection.getItemAt(soundVOArrayCollection.length-1)));
			
			this._soundVO=SoundVO(soundVOArrayCollection.getItemAt(soundVOArrayCollection.length-1));
		}
		
		public function get diskPathsProxy ():DiskPathsProxy { return facade.retrieveProxy (DiskPathsProxy.NAME) as DiskPathsProxy; }
		public function get takeProxy ():ShotTimeLineProxy { return facade.retrieveProxy (ShotTimeLineProxy.NAME) as ShotTimeLineProxy; }
	}
}