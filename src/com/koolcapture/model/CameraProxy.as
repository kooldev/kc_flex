package com.koolcapture.model
{
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class CameraProxy extends Proxy implements IProxy {
		public static const NAME:String = "CameraProxy";// Nom du proxy
		public var cameraIsActive:Boolean = false;
		
		/*	Constructeur */
		public function CameraProxy(data:Object=null) {
			super(NAME, data);
		}
	}
}