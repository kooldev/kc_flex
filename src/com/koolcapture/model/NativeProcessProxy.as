package com.koolcapture.model
{
	import com.koolcapture.tools.Primitive;
	
	import flash.desktop.NativeProcessStartupInfo;
	import flash.filesystem.File;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class NativeProcessProxy extends Proxy implements IProxy {
		public static const NAME:String = "NativeProcessProxy";
		
		/*	Constructeur */
		public function NativeProcessProxy(data:Object=null) {
			super(NAME, data);
		}
		
		public function ffmpegProcessStartupInfo():NativeProcessStartupInfo{
			var _nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			var ffmpeg:String=""
			if ( Primitive.os() == Primitive.MAC ) { 
				ffmpeg="assets/native_apps/ffmpeg";
			}else{
				ffmpeg="assets/native_apps/ffmpeg.exe";
			}
			var file:File = File.applicationDirectory.resolvePath(ffmpeg); 
			
			_nativeProcessStartupInfo.executable = file;
			return _nativeProcessStartupInfo;
		}
	}
}
