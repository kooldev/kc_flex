package com.koolcapture.model {
	import com.koolcapture.constant.ShotConstant;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	public class TimelineEditionProxy extends Proxy implements IProxy {
		public static const NAME:String = "TimelineEditionProxy";
		private var _framesReversable:Boolean=false;
		private var _framesDeletable:Boolean=false;
		private var _framesCutAndCopyable:Boolean=false;
		private var _framesPastable:Boolean=false;
		private var _hasCopiedFrames:Boolean=false;
		
		/*	Constructeur */
		public function TimelineEditionProxy(data:Object=null) { super(NAME, data); }
		
		public function reset():void {
			this.framesReversable=false;
			this.framesDeletable=false;
			this.framesPastable=false;
			this.framesCutAndCopyable=false;
			this.hasCopiedFrames=false;
		}
		
		public function set framesReversable(framesReversable:Boolean):void {
			_framesReversable=framesReversable;
			sendNotification(ShotConstant.REVERSABLE_FRAMES, _framesReversable);			
		}
		
		public function get framesReversable():Boolean {
			return _framesReversable;
		}
		
		public function set framesDeletable(framesDeletable:Boolean):void {
			_framesDeletable=framesDeletable;
			sendNotification(ShotConstant.DELETABLE_FRAMES, _framesDeletable);			
		}
		
		public function get framesDeletable():Boolean {
			return _framesDeletable;
		}
		
		public function set framesPastable(framesPastable:Boolean):void {
			_framesPastable=framesPastable;
			sendNotification(ShotConstant.PASTABLE_FRAMES, _framesPastable);
		}
		
		public function get framesPastable():Boolean {
			return _framesPastable;
		}
		
		public function set framesCutAndCopyable(framesCutAndCopyable:Boolean):void {
			_framesCutAndCopyable=framesCutAndCopyable;
			sendNotification(ShotConstant.CUT_AND_COPYABLE, framesCutAndCopyable);
		}

		public function get framesCutAndCopyable():Boolean {
			return _framesCutAndCopyable;
		}
		
		public function set hasCopiedFrames(hasCopiedFrames:Boolean):void {
			_hasCopiedFrames=hasCopiedFrames;
		}
		
		public function get hasCopiedFrames():Boolean {
			return _hasCopiedFrames;
		}
	}
}