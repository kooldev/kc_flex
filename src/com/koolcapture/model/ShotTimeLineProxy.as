/**
 KoolCapture
 Animation Film Software
 Copyright (c) 2015 lamenagerie.
 Conceived by Kolja Saksida and John Barrie 
 Coded by John Barrie  
 Forked from on Hyde Stop Motion
    
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU LESSER GENERAL PUBLIC LICENSE for more details.
 
 You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.koolcapture.model {
	import com.koolcapture.constant.ProjectConstant;
	import com.koolcapture.constant.ShotConstant;
	import com.koolcapture.event.JJTimerEvent;
	import com.koolcapture.model.vo.FrameFileVO;
	import com.koolcapture.model.vo.FrameVO;
	
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	
	public class ShotTimeLineProxy extends AbstractTimeLineProxy {
		public static const NAME:String = "TakeTimeLineProxy";		// Nom du proxy
		public var includeLiveView:Boolean=true;
		private var _liveViewframeVO:FrameVO;
		//private var _cameraIsActive:Boolean=false;
		private var skippedCurrentFrameForLive:Boolean;
		private var _memoryMap:Object=new Object();
		/* Constructeur */
		public function ShotTimeLineProxy(proxyName:String=NAME, data:Object=null) {
			super(proxyName, data);
			this.frameVOArrayCollection = new ArrayCollection ();
			//var cameraImage:File=File.applicationDirectory.resolvePath("/assets/pictos/back_out.png");
			var cameraImage:File=File.applicationDirectory.resolvePath("assets/pictos/camera_thumb.png");
			
			var frameFileVO:FrameFileVO = new FrameFileVO(cameraImage.url, cameraImage.url, cameraImage.url);
			frameFileVO.loadThumb();
			_liveViewframeVO = new FrameVO (ProjectConstant.LIVE_VIEW_FRAMEVO, frameFileVO);
		}
		
		/* Gestion des tick d'horloge de la timeline */
		override protected function onTimerTick(te:JJTimerEvent):void {
			if (currentIndex + 1 < _frames.length) {
				// trace("less than total frames")
				setCurrentFrame (currentIndex+1);
			}else if (currentIndex + 1==_frames.length && includeLiveView && !skippedCurrentFrameForLive) {
				// trace("more than total frames")
				sendNotification (ShotConstant.LIVE_VIDEO_SHOWN);
				skippedCurrentFrameForLive=true;
				setCurrentFrame (currentIndex + 1);
		    } else {
				// trace("End if the line ");
				skippedCurrentFrameForLive=false;
				if (this._loop) {
					currentIndex = 0;
					sendNotification (ShotConstant.LIVE_VIDEO_HIDDEN);
					setCurrentFrame (currentIndex, true);
				} else {
					setCurrentFrame (currentIndex, true);
					sendNotification (ShotConstant.TRANSPORT_STOP);
				}
			}
		}
		
		override public function set frameVOArrayCollection (framesArrayCollection:ArrayCollection):void{
			super.frameVOArrayCollection=framesArrayCollection;
			_memoryMap=new Object();
		}
		
		override public function play():void {
			skippedCurrentFrameForLive=false;
			
			super.play();
			soundProxy.play();
		}
		
		override public function stop():void {
			soundProxy.stop();
			super.stop();
		}
		
		override public function setCurrentFrame(index:int, force:Boolean = false):void {
			super.setCurrentFrame(index, force);
			this.playBackFlush();
		}
		
		public function playBackFlush():void {
			if(currentIndex>4){
				var frameVO:FrameVO=_frames.getItemAt(currentIndex-3) as FrameVO;
				if(_memoryMap[frameVO.id]<currentIndex)
					frameVO.flushMemory(preferencesProxy.playbackQuality);
			}
		}
		
		/* Recupère la frame a l'index souhaité */
		public function getFrame(index:int):FrameVO {
			try {
				return  _frames.getItemAt(index) as FrameVO;
			} catch(error:Error) { }
			return null;
		}
		
		public function getCurrentFrame():FrameVO {
			return 	_frames.getItemAt(currentIndex)  as FrameVO;
		}
		
		/* Ajoute une frame en fin de liste */
		public function addFrame(frame:FrameVO, number:int = 1):int {
			this.removeCameraFrameVO();
			for (var i:int = 0; i<number; i++){
				_frames.addItem(frame);
				_memoryMap[frame.id]=_frames.length-1;
			}
			
			addCameraFrameVOIfNeeded();
			sendNotification(ShotConstant.SAVE_DATA_TO_DISK);
			return _frames.length -1; // renvoyer l'indice
		}

		/* Insert une frame dans la liste */
		public function addFrameAt(frame:FrameVO, indice:int):int {
			_memoryMap[frame.id]=indice;
			if(this.removeCameraFrameVO()){
				if (indice > _frames.length) 
					return addFrame(frame);
			} else {
				if (indice >= _frames.length) 
					return addFrame(frame);
			}
			_frames.addItemAt(frame, indice);
			
			addCameraFrameVOIfNeeded();
			sendNotification(ShotConstant.SAVE_DATA_TO_DISK);
			return indice;
		}
		
		/* Supprime la liste de frames ciblés */
		public function removeFrames(indices:Vector.<int>):void {
			var revertedIndices:Vector.<int> = indices.concat(); // reclasse les indices du plus gros au plus petit pour eviter les blagues 
			revertedIndices = revertedIndices.sort(Array.NUMERIC | Array.DESCENDING);
			for(var i:int=0; i<revertedIndices.length; i++){
				_frames.removeItemAt(revertedIndices[i]);
			}
			sendNotification(ShotConstant.SAVE_DATA_TO_DISK);
		}
		
		public function initData (xml:XMLList):void {
			this.frameVOArrayCollection = new ArrayCollection ();
			
			if (xml.layer.image.length()==0) {
				addCameraFrameVOIfNeeded();
				return;
			}
			
			var uniqueFrameVO:Object= new Object();
			var frameIndex:int=0;
			for each (var frame:XML in xml.layer.image){
				var frameVO:FrameVO;
				if (uniqueFrameVO[frame.attribute("id")]) {
					frameVO=uniqueFrameVO[frame.attribute("id")];
				} else {
					var frameFileService:FrameFileVO = diskPathsProxy.getFrameFileVO (frame.attribute("id"));
					frameVO = new FrameVO ( frame.attribute("id"), frameFileService );
					uniqueFrameVO[frame.attribute("id")]=frameVO;
				} 
				_memoryMap[frameVO.id]=frameIndex;
				
				this.frameVOArrayCollection.addItem(frameVO);
				frameIndex++;
			}
			
			addCameraFrameVOIfNeeded();
		}
		
		public function addCameraFrameVOIfNeeded():void{
			if( cameraProxy.cameraIsActive){
				this.addCameraFrameVO();	
			}
		}
		
		public function addCameraFrameVO():void {
			if ( this.frameVOArrayCollection.getItemIndex(this._liveViewframeVO) != -1 ) {
				this.frameVOArrayCollection.removeItemAt((_frames.length-1));
			}
			this.frameVOArrayCollection.addItem(this._liveViewframeVO);
			currentFrameChanged(this.frameVOArrayCollection.length-1);
		}
		
		public function removeCameraFrameVO():Boolean {
			var liveViewIndex:int = _frames.getItemIndex(this._liveViewframeVO);
			if ( liveViewIndex == (_frames.length-1) && liveViewIndex != -1 ) {
				this.frameVOArrayCollection.removeItemAt((_frames.length-1));
				return true;
			}
			return false;
		}
		
		final override protected function currentFrameChanged(index:int):void{ 
			sendNotification(ShotConstant.CURRENT_FRAME_CHANGED, index);
		}
		
		final override protected function gotoFirstFrame():void{
			sendNotification(ShotConstant.GOTO_FIRST_FRAME, _frames);
		}
		
		public function get soundProxy():SoundProxy {
			return facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;;
		}
		
		public function get diskPathsProxy():DiskPathsProxy {
			return facade.retrieveProxy(DiskPathsProxy.NAME) as DiskPathsProxy;;
		}
		
		public function get cameraProxy():CameraProxy {
			return facade.retrieveProxy(CameraProxy.NAME) as CameraProxy;;
		}
	}
}